# encoding: utf-8
from gnr.core.gnrdecorator import metadata, public_method
from gnr.core.gnrbag import Bag
from gnr.core.gnrnumber import decimalRound


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "cane_valutazione_tipo",
            pkey="id",
            name_long="!!Tipi valutazione",
            name_plural="!!Tipi valutazione",
            caption_field="titolo",
        )
        self.sysFields(tbl, df=True)
        tbl.column("titolo", name_long="!!Titolo")

    @metadata(mandatory=True)
    def sysRecord_ETOGRAMMA(self):
        return self.newrecord(titolo="Etogramma")

    @public_method
    def calcolaRisultato(self, pkey, dati_valutazione, **kwargs):
        record_tipo = self.cachedRecord(pkey)
        if record_tipo.get("__syscode"):
            handler = getattr(self, "calcolo_%(__syscode)s" % record_tipo, None)
            if not handler:
                return
            return handler(record_tipo, dati_valutazione)

    def calcolo_ETOGRAMMA(self, record, dati_valutazione):
        if not dati_valutazione:
            return
        b = Bag(record["df_fields"])
        s = []
        for k, v in b.digest("#k,#v.source_checkboxtext"):
            v = list(filter(lambda r: r != "/", v.split("\n")))
            valutazione = dati_valutazione[k]
            if not valutazione:
                continue
            s.append(
                decimalRound(
                    decimalRound(len(valutazione.split("\n"))) / decimalRound(len(v))
                )
            )
        result = sum(s)
        return result
