# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "pg_unita_staff",
            pkey="id",
            name_long=u"!!Unità staff",
            name_plural="!!Staff coinvolti",
        )
        self.sysFields(tbl, counter="staff_id")
        tbl.column("staff_id", size="22", group="_", name_long="!!Staff").relation(
            "staff.id",
            relation_name="attivita_programma",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column("unita_id", size="22", group="_", name_long=u"!!Unità").relation(
            "pg_unita.id",
            relation_name="staff_coinvolti",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column("responsabile", dtype="B", name_long="!!Responsabile")
        tbl.column(
            "automobile", dtype="B", name_long="!!Porta automobile", name_short="!!Auto"
        )
        tbl.column(
            "diario", dtype="B", name_long="!!Compila diario", name_short="!!Diario"
        )
        tbl.column("note", name_long="!!Note")
        tbl.aliasColumn("data", "@unita_id.data")
        tbl.aliasColumn("ora_inizio", "@unita_id.ora_inizio")
        tbl.aliasColumn("ora_fine", "@unita_id.ora_fine")
        tbl.aliasColumn("volontario", "@staff_id.nome_flat")
        tbl.aliasColumn("cane", "@unita_id.@cane_id.nome", name_long="Cane")
        tbl.aliasColumn("area", "@unita_id.@area_id.nome", name_long="Area")
        tbl.aliasColumn("colore_unita", "@unita_id.colore", name_long="Colore unita")
        tbl.aliasColumn("programma_id", "@unita_id.programma_id")
        tbl.aliasColumn("volontari", "@unita_id.volontari")
        tbl.aliasColumn("volontari_full", "@unita_id.volontari_full")
        tbl.aliasColumn("unita_descrizione", "@unita_id.descrizione")
        tbl.aliasColumn("attivita", "@unita_id.@tipo_attivita_id.descrizione")

        tbl.formulaColumn(
            "volontario_note",
            """(CASE WHEN $responsabile IS TRUE THEN '(*)'  ELSE '' END) ||
                                                (CASE WHEN $automobile IS TRUE THEN '(AUTO)'  ELSE '' END) ||
                                                COALESCE($note,'')""",
        )

    def trigger_onDeleted(self, record):
        if not self.currentTrigger.parent:
            self.db.deferToCommit(
                self.db.table("bau.pg_unita").checkEmpty,
                unita_id=record["unita_id"],
                _deferredId=record["unita_id"],
            )
