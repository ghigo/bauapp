# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "assemblea_staff",
            pkey="id",
            name_long="!!Partecipante",
            name_plural="!!Partecipanti",
        )
        self.sysFields(tbl)
        tbl.column(
            "staff_id", size="22", group="_", name_long="!!Partecipante"
        ).relation(
            "staff.id", relation_name="aseemblee", mode="foreignkey", onDelete="raise"
        )
        tbl.column(
            "assemblea_id", size="22", group="_", name_long="!!Riunione"
        ).relation(
            "assemblea.id",
            relation_name="partecipanti",
            mode="foreignkey",
            onDelete="cascade",
        )
