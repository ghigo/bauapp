# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "pg_pappa",
            pkey="id",
            name_long="!!Distribuzione pappe",
            name_plural="!!Distribuzione pappe",
        )
        self.sysFields(tbl, counter="programma_id")
        tbl.column(
            "programma_id", size="22", group="_", name_long="!!Programma"
        ).relation(
            "pg_programma.id",
            relation_name="pappe",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column("cane_id", size="22", group="_", name_long="!!Cane").relation(
            "cane.id", mode="foreignkey", onDelete="raise"
        )
        tbl.column("struttura_id", size="22", group="_", name_long="!!Dove").relation(
            "struttura.id", mode="foreignkey", onDelete="raise"
        )
        tbl.column("razione", name_long="!!Razione")
        tbl.column("terapia", name_long="!!Terapia")
