# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "cane_razza",
            pkey="id",
            name_long="Razza",
            name_plural="Razze",
            caption_field="nome",
        )
        self.sysFields(tbl, hierarchical="nome", df=True, counter=True)
        tbl.column("nome", size=":50", name_long="nome", name_short="Nome razza")
        tbl.column("descrizione", name_long="descrizione", name_short="Descrizione")
        tbl.column(
            "taglia", size=":1", name_long="Taglia", values="1:Piccola,2:Media,3:Grande"
        )
