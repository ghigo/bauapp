# encoding: utf-8


class Table(object):
    """cambiare il sistema cane stato in cane storico
        che va a modificare la situazione direttamente la situazione del cane
        campi unmodifiable che creano storico alla modifica
        tbl prescrizione
        quando confermo esplode in righe assunzioni 
        """

    def config_db(self, pkg):
        tbl = pkg.table("cane", pkey="id", name_long="Cane", name_plural="Cani", 
                        caption_field="nome",audit="lazy", full_upd=True,
                        partition_associazione_codice='associazione_codice')
        self.sysFields(tbl)
        tbl.column("nome",size=":40",name_long="Nome",name_short="Nome",validate_notnull=True,validate_case="C",)
        tbl.column("data_nascita", dtype="D", name_long="Nato il")
        tbl.column("data_nascita_esatta", dtype="B", name_long="!!D.Nascita esatta")

        tbl.column("sesso", size=":1", name_long="!![it]Sesso", values="M:Maschio,F:Femmina")
        tbl.column("data_sterilizzazione", dtype="D", name_long="!!Data sterilizzazione", name_short="Sterilizzazione")
        tbl.column("razza_id", size="22", group="_", name_long="!!Razza"
                    ).relation("cane_razza.id", relation_name="cani", mode="foreignkey", onDelete="raise")

        tbl.column("altezza", dtype="L", name_long="!!Altezza(cm)")
        tbl.column("peso", dtype="N", name_long="!!Peso(Kg)")

        tbl.column("taglia", size=":1", name_long="Taglia", values="1:Piccola,2:Media,3:Grande")
        tbl.column("microchip", size=":15", name_long="!!Microchip")
        tbl.column("foto", name_long="Foto")
        tbl.column("data_arrivo", dtype="D", name_long="Data arrivo")

        tbl.column("associazione_codice", size=":10", name_long="Associazione"
                    ).relation("associazione.codice", relation_name="cani", mode="foreignkey")
        tbl.column('pensione_id',size='22', group='_', name_long='Pensione'
                    ).relation('pensione.id', relation_name='cani', mode='foreignkey', onDelete='raise')
        tbl.column("struttura_id", size="22", group="_", name_long="!!Locazione"
                  ).relation("struttura.id", relation_name="cani", mode="foreignkey", onDelete="setnull")
        tbl.column("bollino_codice", size=":10", name_long="Bollino"
            ).relation("bollino.codice", mode="foreignkey", relation_name="cani")
        tbl.column("bollino_dati",dtype="X",name_long="dati bollino", subfields="bollino_codice",)

        tbl.column("dieta_mattino", name_long="!!Mangime mattino")
        tbl.column("dieta_sera", name_long="!!Mangime sera")

        tbl.column("terapia_mattino", name_long="!!Terapia mattino")
        tbl.column("terapia_sera", name_long="!!Terapia sera")

        tbl.column("info_provenienza", name_long="!!Provenienza", name_short="!!Proviene da")
        tbl.column("info_stallo", name_long="!!Si trova a", name_short="!!Stallo")
        tbl.column("info_carattere", name_long="!!Carattere", name_short="!!Carattere")
        tbl.column("info_compatibilita",name_long=u"!!Compatibilità",name_short=u"!!Compatibilità")
        tbl.column("info_storia", name_long="!!La sua storia", name_short="!!Storia")

        tbl.column("info_provenienza", name_long="!!Provenienza", name_short="!!Proviene da")
        tbl.column("info_stallo", name_long="!!Si trova a", name_short="!!Stallo")
        tbl.column("info_carattere", name_long="!!Carattere", name_short="!!Carattere")
        tbl.column("info_compatibilita",name_long=u"!!Compatibilità",name_short=u"!!Compatibilità",)
        tbl.column("info_storia", name_long="!!La sua storia", name_short="!!Storia")

        tbl.column("info_anamnesi", name_long="!!Anamnesi", name_short="!!Anamnesi")
        tbl.column("info_salute", name_long="!!Salute", name_short="!!Salute")

        tbl.column("esame_sierologico_codice",size=":40",sql_value="'SIEROLOGIA'",group="_",name_long="!!Esame sierologico",
        ).relation("esame.codice", mode="foreignkey", onDelete="raise")

        tbl.column("sierologia",dtype="X",name_long="!!Sierologia",subfields="esame_sierologico_codice")

        tbl.column("come_agire", name_long="!!Come agire", name_short="!!Come agire")

        tbl.formulaColumn("eta","extract(YEAR FROM age($data_nascita))",dtype="L",name_long=u"!!Età",)
        tbl.column("sterilizzato", dtype="B")  # old
        tbl.column("in_adozione_dal", dtype="D", name_long="!!In adozione dal")
