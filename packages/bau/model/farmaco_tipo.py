# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "farmaco_tipo",
            pkey="id",
            name_long="!!Tipo Farmaco",
            name_plural="!!Tipi Farmaco",
            caption_field="descrizione",
        )
        tbl.column("descrizione", name_long="!!Descrizione")
        tbl.column(
            "categoria",
            size="2",
            values="""AP:Antiparassitari,AB:Antibiotici,AI:Anti infiammatori,O:Altro""",
            name_long="!!Categoria farmaco",
        )
        self.sysFields(
            tbl, hierarchical="descrizione", hierarchical_root_id=True, counter=True
        )
