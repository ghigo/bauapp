# encoding: utf-8
from gnr.core.gnrdecorator import metadata


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "esame",
            pkey="codice",
            name_long="!!Esame",
            name_plural="!!Esami",
            caption_field="nome",
        )
        self.sysFields(tbl, df=True, id=False)
        tbl.column("codice", size=":40", name_long="!!Codice", validate_case="u")
        tbl.column("descrizione", name_long="!!Descrizione")

    @metadata(mandatory=True)
    def sysRecord_SIEROLOGIA(self):
        return self.newrecord(titolo="Sierologia")
