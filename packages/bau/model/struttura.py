# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "struttura",
            pkey="id",
            name_long="Struttura",
            name_plural="Struttura",
            caption_field="hierarchical_nome",
        )
        self.sysFields(tbl, hierarchical="nome", counter=True)
        tbl.column('pensione_id',size='22', group='_', name_long='Pensione'
                    ).relation('pensione.id', relation_name='strutture', mode='foreignkey', onDelete='raise')
        tbl.column("nome", name_long="Nome")
        tbl.column("descrizione", name_long="!!Descrizione")
        tbl.column("n_cani_entro_10", dtype="L", name_long="!!N. Cani entro 10kg")
        tbl.column("n_cani_10_25", dtype="L", name_long="!!N. Cani da 10kg a 25kg")
        tbl.column("n_cani_25_oltre", dtype="L", name_long="!!N. Cani oltre 25kg")



    def onLoading_defaultPensione(self,record, newrecord, loadingParameters, recInfo):
        if newrecord:
            record['pensione_id'] = record['@parent_id.pensione_id'] or self.db.currentEnv.get('current_pensione_id')
        

    def trigger_onInserting(self,record):
        record['pensione_id'] = record['pensione_id'] or self.db.currentEnv.get('current_pensione_id')