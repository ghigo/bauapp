# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "banchetto",
            pkey="id",
            name_long="!!Banchetto",
            name_plural="!!Banchetti",
            caption_field="nome",
        )
        self.sysFields(tbl)
        tbl.column("evento_id", size="22", group="_", name_long="!!Evento").relation(
            "evento.id",
            relation_name="banchetti",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column("nome", name_long="!!Nome")
        tbl.column("note", name_long="!!Note")
        tbl.column("ora_inizio", dtype="H", name_long="!!Dalle")
        tbl.column("ora_fine", dtype="H", name_long="!!Alle")
        tbl.aliasColumn("data", "@evento_id.data", dtype="D")
        tbl.formulaColumn(
            "ora_inizio_disponibile",
            "COALESCE($ultima_ora_fine,$ora_inizio)",
            dtype="H",
        )
        tbl.formulaColumn(
            "ultima_ora_fine",
            select=dict(
                table="bau.banchetto_turno",
                columns="$ora_fine",
                limit=1,
                order_by="$ora_fine desc",
                where="$banchetto_id=#THIS.id AND $ora_fine<#THIS.ora_fine",
            ),
            dtype="H",
        )
