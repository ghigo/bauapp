# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "farmaco",
            pkey="id",
            name_long="!!Farmaco",
            name_plural="!!Farmaco",
            caption_field="caption_farmaco",
        )
        self.sysFields(tbl)
        tbl.column("descrizione", name_long="!!Descrizione")
        tbl.column(
            "farmaco_tipo_id", size="22", group="_", name_long="!!Tipo"
        ).relation(
            "farmaco_tipo.id",
            relation_name="farmaci",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column("formato", size=":40", name_long="!!Formato")
        tbl.column("prescrizione", dtype="B", name_long="!!Prescrizione")
        tbl.formulaColumn(
            "caption_farmaco", "@farmaco_tipo_id.descrizione || ' ' || $descrizione"
        )
