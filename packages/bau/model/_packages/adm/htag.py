# encoding: utf-8
from gnr.core.gnrdecorator import metadata


class Table(object):
    @metadata(mandatory=True)
    def sysRecord_org(self):
        return self.newrecord(
            code="org", description="Organizzazione", hierarchical_code="org"
        )

    @metadata(mandatory=True)
    def sysRecord_staff(self):
        return self.newrecord(
            code="staff", description="Staff", hierarchical_code="staff"
        )

    @metadata(mandatory=True)
    def sysRecord_trainer(self):
        return self.newrecord(
            code="trainer", description="Trainer", hierarchical_code="trainer"
        )

    @metadata(mandatory=True)
    def sysRecord_vet(self):
        return self.newrecord(
            code="vet", description="Veterinario", hierarchical_code="veterinario"
        )
