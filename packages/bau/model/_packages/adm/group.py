# encoding: utf-8
from gnr.core.gnrdecorator import metadata


class Table(object):
    @metadata(mandatory=True)
    def sysRecord_org(self):
        return self.newrecord(code="org", description="Organizzazione")

    @metadata(mandatory=True)
    def sysRecord_vol(self):
        return self.newrecord(code="vol", description="Volontario")

    @metadata(mandatory=True)
    def sysRecord_est(self):
        return self.newrecord(code="est", description="Esterno")

    @metadata(mandatory=True)
    def sysRecord_dev(self):
        return self.newrecord(code="dev", description="Sviluppatore")
