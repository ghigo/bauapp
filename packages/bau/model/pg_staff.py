# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "pg_staff",
            pkey="id",
            name_long="!!Programma staff",
            name_plural="!!Programmi staff",
        )
        self.sysFields(tbl, counter="programma_id")
        tbl.column("staff_id", size="22", group="_", name_long="!!Staff").relation(
            "staff.id",
            relation_name="staff_programmi",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column(
            "programma_id", size="22", group="_", name_long="!!Programma"
        ).relation(
            "pg_programma.id",
            relation_name="staff_coinvolti",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column("ora_inizio", dtype="H", name_long="!!Ora inizio")
        tbl.column("ora_fine", dtype="H", name_long="!!Ora fine")
        tbl.column(
            "automobile", dtype="B", name_long="!!Porta automobile", name_short="!!Auto"
        )
        tbl.column(
            "diario", dtype="B", name_long="!!Compila diario", name_short="!!Diario"
        )
        tbl.column("note", name_long="!!Note")

        tbl.formulaColumn(
            "ora_inizio_calc",
            "COALESCE($ora_inizio,@programma_id.ora_inizio)",
            dtype="H",
            name_long="!!Ora inizio",
        )
        tbl.formulaColumn(
            "ora_fine_calc",
            "COALESCE($ora_fine,@programma_id.ora_fine)",
            dtype="H",
            name_long="!!Ora fine",
        )

        tbl.aliasColumn("volontario", "@staff_id.nome_flat")

    def trigger_onDeleted(self, record):
        self.db.table("bau.pg_unita_staff").deleteSelection(
            where="$staff_id=:sid AND $programma_id=:pgid",
            sid=record["staff_id"],
            pgid=record["programma_id"],
        )
