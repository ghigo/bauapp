# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "cane_terapia", pkey="id", name_long="!!Terapia", name_plural="!!Terapie"
        )
        self.sysFields(tbl)
        tbl.column("data_inizio", dtype="D", name_long="!!Data inizio")
        tbl.column("data_fine", dtype="D", name_long="!!Data fine")

        tbl.column("cane_id", size="22", group="_", name_long="!!Cane").relation(
            "cane.id", relation_name="terapie", mode="foreignkey", onDelete="cascade"
        )
        tbl.column("farmaco_id", size="22", group="_", name_long="!!Farmaco").relation(
            "farmaco.id", mode="foreignkey", onDelete="raise"
        )
        tbl.column("dosaggio", name_long="!!Dosaggio")
        tbl.column("orario", name_long="!!Orario")
        tbl.column("frequenza", dtype="L", name_long="!!Frequenza")
        tbl.formulaColumn(
            "trattamento_corrente",
            "($data_fine IS NULL OR $data_fine<=:env_workdate)",
            dtype="B",
        )
