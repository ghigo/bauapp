# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "livello",
            pkey="codice",
            name_long="Livello",
            name_plural="Livelli",
            caption_field="nome",
            lookup=True,
        )
        self.sysFields(tbl, counter=True)
        tbl.column("codice", size=":10", name_long="Codice", unique=True, indexed=True)
        tbl.column("nome", size=":30", name_long="Nome")
        tbl.column("descrizione", name_long="!!Descrizione")
        tbl.column(
            "bollino_max", size=":10", group="_", name_long="!!Bollino cane"
        ).relation("bollino.codice", mode="foreignkey", onDelete="setnull")
