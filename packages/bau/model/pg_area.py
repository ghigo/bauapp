# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "pg_area",
            pkey="id",
            name_long="!!Area cani",
            name_plural="!!Aree cani",
            caption_field="nome",
            lookup=True,
        )
        self.sysFields(tbl)
        tbl.column("nome", size=":30", name_long="!!Nome")
        tbl.column("descrizione", name_long="!!Descrizione")
        tbl.column("locazione", name_long="!!Locazione", values="pensione,esterna")
        tbl.column("automobile", dtype="B", name_long="!!Richiede auto")
