# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "cane_vaccinazione",
            pkey="id",
            name_long="!!Cane vaccinazione",
            name_plural="!!Cane vaccinazioni",
            order_by='$data'
        )
        self.sysFields(tbl)
        tbl.column("data", dtype="D", name_long="!!Data")
        tbl.column("cane_id", size="22", group="_", name_long="!!Cane").relation(
            "cane.id",
            relation_name="cane_vaccinazioni",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column("vaccino_id", size="22", group="_", name_long="!!Vaccino").relation(
            "vaccino.id",
            relation_name="vaccinazione_cani",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column("note", name_long="!!Note")
