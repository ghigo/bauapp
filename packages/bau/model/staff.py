# encoding: utf-8
from gnr.core.gnrdecorator import metadata,public_method
from gnr.core.gnrbag import Bag

class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('staff',pkey='id',name_long='Staff',
                        name_plural='Staff',caption_field='nome',
                        partition_associazione_codice='associazione_codice')
        self.sysFields(tbl)
        self.anagraficaAliases(tbl)
        tbl.column('nome',size=':50',name_long='Nome')
        tbl.column('user_id',size='22',name_long='user_id').relation('adm.user.id',mode='foreignkey',relation_name='baustaff', one_one=True)
        tbl.column('data_richiesta', dtype='D', name_long='!!Data richiesta iscrizione', name_short='!!In prova dal')
        tbl.column('data_accettazione', dtype='D', name_long='!!Data accettazione', name_short='!!Approvato dal')
        tbl.column('data_iscrizione',dtype='D',name_long='Iscrittizione definitiva')
        tbl.column('telefono',name_long='Telefono')
        tbl.column('gdpr',dtype='B',name_long='GDPR Firmato')
        tbl.column('quota_versata', dtype='B', name_long='!!Quota versata')
        tbl.column('tessera_numero', size=':30', name_long='!!Tessera n.')
        tbl.column('tipo_socio', size=':3',values='SC:Socio,FO:Socio Fondatore,ON:Socio onorario', name_long='!!Tipo socio')
        tbl.column('volontario', dtype='B', name_long='!!Volontario attivo', name_short='!!Volontario')
        tbl.column('anagrafica_id',size='22',name_long='Dati anagrafici').relation('anagrafica.id',mode='foreignkey', one_one=True)
        tbl.column('associazione_codice',size=':10',name_long='Associazione').relation('associazione.codice',relation_name='staff')
        tbl.column('livello_codice',size=':10',name_long='Livello').relation('livello.codice')
        tbl.column('foto',name_long='Foto')
        tbl.column('note', name_long='!!Note')
        tbl.aliasColumn('bollino','@livello_codice.bollino_max')
        tbl.formulaColumn('cani',"array_to_string(ARRAY(#cani),',')",select_cani=dict(columns="@cane_id.nome",table='bau.cane_staff',
                                           where='$staff_id=#THIS.id AND @cane_id.__del_ts IS NULL'))

        tbl.formulaColumn('nome_flat',"regexp_replace($nome,'[.|,|;]', '', 'g')")
        tbl.formulaColumn('pg_orario_invalido',exists=dict(table='bau.pg_unita_staff',
                                                           where="""$id=#THIS.id AND $unita_id!=:unita_id AND 
                                                                    ($ora_inizio BETWEEN :ora_inizio AND :ora_fine OR 
                                                                    $ora_fine BETWEEN :ora_inizio AND :ora_fine) """),
                                                                    dtype='B')
    
    @metadata(doUpdate=True)
    def touch_creaUserDaVolontari(self,record,old_record=None):
        if not record['user_id']:
            self.createNewUser(record)


    def createNewUser(self,record=None,**kwargs):
        anagrafica = self.db.table('bau.anagrafica').record(pkey=record['anagrafica_id']).output('dict')
        rec = Bag()
        rec['username'] = record['nome'].replace('.','').replace(' ','_').lower()
        rec['email'] = anagrafica['email']
        rec['lastname'] = anagrafica['cognome']
        rec['firstname'] = anagrafica['nome']
        rec['status'] = 'new'
        rec['group_code'] = 'vol'
        self.db.table('adm.user').insert(rec)
        page = self.db.currentPage
        rec['link'] = page.externalUrlToken(page.site.homepage, userid=rec['id'],max_usages=1)
        rec['greetings'] = rec['firstname'] or rec['username']
        email = rec['email']
        body = u"""Ciao $greetings ora puoi usare la BauApp di RescueBau. 
        Il tuo utente è $username, seguendo questo link $link ti verrà richiesto inserire una password.
        Ciao e benvenuto in BauApp"""
        page.getService('mail').sendmail_template(rec,to_address=email,
                            body=body, subject='Richiesta conferma utente',
                            _async=False,html=True)
        record['user_id'] = rec['id']
        
