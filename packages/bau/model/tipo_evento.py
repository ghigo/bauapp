# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "tipo_evento",
            pkey="codice",
            name_long="Tipo evento",
            name_plural="Tipi evento",
            caption_field="descrizione",
            lookup=True,
        )
        self.sysFields(tbl, counter=True)
        tbl.column("codice", size=":10", name_long="Codice", unique=True, indexed=True)
        tbl.column("descrizione", size=":30", name_long="Nome")
        tbl.column("colore", name_long="Colore", cell_edit=dict(tag="colorTextBox"))
        tbl.column("annotazioni", name_long="!!Note")
