# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "cane_valutazione",
            pkey="id",
            name_long="!!Valutazione",
            name_plural="!!Valutazioni",
            caption_field="valutazione_titolo",
        )
        self.sysFields(tbl)
        tbl.column("data", dtype="D", name_long="!!Data")
        tbl.column('data_chiusura', dtype='D', name_long='Chiusa il')
        tbl.column("cane_id", size="22", group="_", name_long="!!Cane").relation(
            "cane.id",
            relation_name="valutazioni",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column(
            "valutazione_tipo_id", size="22", group="_", name_long="!!Tipo"
        ).relation("cane_valutazione_tipo.id", mode="foreignkey", onDelete="raise")
        tbl.column(
            "dati_valutazione",
            dtype="X",
            name_long="!!Dati valutazione",
            subfields="valutazione_tipo_id",
            _sendback=True,
        )
        tbl.column(
            "risultato_valutazione", dtype="money", name_long="!!Risultato valutazione"
        )
        tbl.column("resoconto", name_long="!!Resoconto")
        tbl.aliasColumn("valutazione_titolo", "@valutazione_tipo_id.titolo")
        tbl.formulaColumn(
            "passata",
            dtype="B",
            exists=dict(
                table="bau.cane_valutazione",
                where="$cane_id=#THIS.cane_id AND $valutazione_tipo_id=#THIS.valutazione_tipo_id AND $data>#THIS.data",
            ),
        )

        tbl.formulaColumn(
            "passata_in_data",
            dtype="B",
            exists=dict(
                table="bau.cane_valutazione",
                where="$cane_id=#THIS.cane_id AND $valutazione_tipo_id=#THIS.valutazione_tipo_id AND $data>#THIS.data AND $data<=:env_data_limite",
            ),
        )

    def trigger_onInserted(self, record):
        self.deferredUpdateParentFullTs(record=record, relation_field="cane_id")

    def trigger_onUpdated(self, record, old_record):
        self.deferredUpdateParentFullTs(record=record, relation_field="cane_id")

    def trigger_onDeleted(self, record):
        self.deferredUpdateParentFullTs(record=record, relation_field="cane_id")
