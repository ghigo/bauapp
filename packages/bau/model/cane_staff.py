# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "cane_staff", pkey="id", name_long="!!Staff", name_plural="!!Cane staff"
        )
        self.sysFields(tbl, counter="staff_id")
        tbl.column("staff_id", size="22", group="_", name_long="!!Staff").relation(
            "staff.id",
            relation_name="cani_associati",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column("cane_id", size="22", group="_", name_long="!!Cane").relation(
            "cane.id",
            relation_name="staff_responsabili",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column(
            "ruolo",
            size=":1",
            name_long="!!Ruolo",
            values="1:Volontario,2:Educatore,3:Valutatore",
        )
        tbl.column("annotazioni", name_long="!!Note")
