# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "banchetto_turno_staff",
            pkey="id",
            name_long="!!Banchetto turno staff",
            name_plural="Staff",
        )
        self.sysFields(tbl, counter="turno_id")
        tbl.column("turno_id", size="22", group="_", name_long="!!Turno").relation(
            "banchetto_turno.id",
            relation_name="staff_turno",
            mode="foreignkey",
            deferred=True,
            onDelete="cascade",
        )
        tbl.column("staff_id", size="22", group="_", name_long="!!Staff").relation(
            "staff.id", relation_name="turni_staff", mode="foreignkey", onDelete="raise"
        )

        tbl.aliasColumn("data", "@turno_id.@evento_id.data")
        tbl.aliasColumn("ora_inizio", "@turno_id.ora_inizio")
        tbl.aliasColumn("ora_fine", "@turno_id.ora_fine")
        tbl.aliasColumn("volontario", "@staff_id.nome_flat")
        tbl.aliasColumn("cane", "@turno_id.@cane_id.nome")
        tbl.aliasColumn("area", "@turno_id.@area_id.nome")
        tbl.aliasColumn("colore_turno", "@turno_id.colore")
        tbl.aliasColumn("evento_id", "@turno_id.evento_id")
        tbl.aliasColumn("volontari", "@turno_id.volontari")
        tbl.aliasColumn(
            "banchetto", "@turno_id.@banchetto_id.nome", name_long="Banchetto"
        )
        tbl.aliasColumn(
            "banchetto_id", "@turno_id.banchetto_id", name_long="Banchetto id"
        )
