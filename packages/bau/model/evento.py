# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "evento",
            pkey="id",
            name_long="!!Evento",
            name_plural="!!Eventi",
            caption_field="titolo",
        )
        self.sysFields(tbl)
        tbl.column("titolo", size=":50", name_long="!!Titolo")
        tbl.column("tipo_evento", size=":10", group="_", name_long="!!Tipo").relation(
            "tipo_evento.codice",
            relation_name="baueventi",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column("data", dtype="D", name_long="!!Data")
        tbl.column("ora_inizio", dtype="H", name_long="!!Dalle")
        tbl.column("ora_fine", dtype="H", name_long="!!Alle")
        tbl.column("luogo", name_long="!!Luogo")
        tbl.column("indirizzo", name_long="!!Indirizzo")
        tbl.column("localita", name_long="!!Localita")
        tbl.column("provincia", size="2", group="_", name_long="!!Provincia").relation(
            "glbl.provincia.sigla", mode="foreignkey", onDelete="raise"
        )
        tbl.column("geocoords", name_long="!!Geocoords")
        tbl.column("descrizione", name_long="!!Descrizione")
        tbl.column("mostra", dtype="B", name_long="!!Mostra a volontari")
        tbl.formulaColumn(
            "disponibilita_id",
            select=dict(
                table="bau.evento_staff",
                columns="$id",
                where="$evento_id=#THIS.id AND $staff_id=:env_staff_id",
            ),
        )
