# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "pg_programma",
            pkey="id",
            name_long="!!Programma giornata",
            name_plural="!!Programmi",
            caption_field="data",
        )
        self.sysFields(tbl)
        tbl.column("data", dtype="D", name_long="!!Data")
        tbl.column("ora_inizio", dtype="H", name_long="!!Ora inizio")
        tbl.column("ora_fine", dtype="H", name_long="!!Ora fine")
        tbl.column(
            "responsabile_id", size="22", group="_", name_long="!!Responsabile"
        ).relation(
            "staff.id",
            relation_name="programmi_responsabile",
            mode="foreignkey",
            onDelete="raise",
        )

        tbl.column('pensione_id',size='22', group='_', name_long='Pensione'
                    ).relation('pensione.id', relation_name='programmi', mode='foreignkey', onDelete='raise')
        tbl.column("associazione_codice", size=":10", name_long="Associazione"
                    ).relation("associazione.codice", relation_name="programmi", mode="foreignkey")

        tbl.column("note", name_long="!!Note")
        tbl.column("indicazioni_pappe", name_long="!!Indicazioni pappe")
        tbl.column("annullato", dtype="B", name_long="!!Annullato")
