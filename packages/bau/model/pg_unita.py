# encoding: utf-8
from gnr.core.gnrdecorator import public_method


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "pg_unita",
            pkey="id",
            name_long=u"!!Unità programma",
            name_plural=u"!!Unità programma",
            broadcast="unita_id",
            caption_field="caption_unita",
        )
        self.sysFields(tbl)
        tbl.column(
            "programma_id", size="22", group="_", name_long="!!Programma"
        ).relation(
            "pg_programma.id",
            relation_name="righe",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column("cane_id", size="22", group="_", name_long="!!Cane").relation(
            "cane.id",
            relation_name="unita_programma",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column(
            "tipo_attivita_id", size="22", group="_", name_long=u"!!Attività"
        ).relation(
            "tipo_attivita.id",
            relation_name="unita_programma",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column("area_id", size="22", group="_", name_long="!!Area").relation(
            "pg_area.id",
            relation_name="unita_programma",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column("descrizione", name_long="!!Descrizione")
        tbl.column("ora_inizio", dtype="H", name_long="!!Ora inizio")
        tbl.column("ora_fine", dtype="H", name_long="!!Ora fine")
        tbl.column("colore", name_long="!!Colore")
        tbl.aliasColumn("data", "@programma_id.data")
        tbl.aliasColumn("area_nome", "@area_id.nome")
        tbl.aliasColumn("cane_nome", "@cane_id.nome")
        tbl.formulaColumn(
            "caption_unita", "@cane_id.nome || '-' || @tipo_attivita_id.descrizione"
        )

        tbl.formulaColumn(
            "n_staff",
            select=dict(
                columns="count(*)",
                table="bau.pg_unita_staff",
                where="$unita_id=#THIS.id",
            ),
            dtype="L",
            group="*",
        )

        tbl.formulaColumn(
            "volontari",
            "array_to_string(ARRAY(#volont),', ')",
            select_volont=dict(
                table="bau.pg_unita_staff",
                columns="$volontario",
                where="$unita_id=#THIS.id",
            ),
        )

        tbl.formulaColumn(
            "volontari_full",
            "array_to_string(ARRAY(#vol),', ')",
            select_vol=dict(
                table="bau.pg_unita_staff",
                columns="$volontario || (CASE WHEN ($volontario_note IS NOT NULL AND $volontario_note!='') THEN ' ' || $volontario_note ELSE '' END)",
                where="$unita_id=#THIS.id",
                order_by="$_row_count",
            ),
        )

        tbl.pyColumn(
            "unita_template",
            dtype="A",
            group="_",
            py_method="templateColumn",
            template_name="unita_template",
        )

    @public_method
    def controlloArea(
        self, value, programma_id=None, ora_inizio=None, ora_fine=None, **kwargs
    ):
        pass

    @public_method
    def controlloCane(
        self,
        value,
        programma_id=None,
        ora_inizio=None,
        ora_fine=None,
        area=None,
        **kwargs
    ):
        pass

    def checkEmpty(self, unita_id=None, **kwargs):
        f = (
            self.db.table("bau.pg_unita_staff")
            .query(where="$unita_id=:uid", uid=unita_id)
            .fetch()
        )
        if not f:
            raise self.exception(
                "business_logic", msg="Non puoi eliminare l'ultimo staff"
            )
