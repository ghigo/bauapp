# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "evento_staff",
            pkey="id",
            name_long="!!Partecipante",
            caption_field="evento_nome",
            name_plural="!!Partecipanti",
        )
        self.sysFields(tbl)
        tbl.column("evento_id", size="22", group="_", name_long="!!Evento").relation(
            "evento.id",
            relation_name="partecipanti",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column("staff_id", size="22", group="_", name_long="!!Staff").relation(
            "staff.id", relation_name="eventi", mode="foreignkey", onDelete="cascade"
        )
        tbl.column("ora_inizio", dtype="H", name_long="!!Dalle")
        tbl.column("ora_fine", dtype="H", name_long="!!Alle")
        tbl.column("note", name_long="!!Note")
        tbl.aliasColumn("evento_nome", "@evento_id.titolo")
