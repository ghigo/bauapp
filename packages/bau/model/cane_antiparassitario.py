# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "cane_antiparassitario",
            pkey="id",
            name_long="!!Cane antiparassitario",
            name_plural="!!Cane antiparassitari",
            order_by='$data'
        )
        self.sysFields(tbl)
        tbl.column("data", dtype="D", name_long="!!Data")
        tbl.column("cane_id", size="22", group="_", name_long="!!Cane").relation(
            "cane.id",
            relation_name="cane_trattamenti_antiparassitari",
            mode="foreignkey",
            onDelete="cascade",
        )
        tbl.column(
            "farmaco_id", size="22", group="_", name_long="!!Antiparassitario"
        ).relation(
            "farmaco.id", relation_name="cani", mode="foreignkey", onDelete="raise"
        )
        tbl.column("note", name_long="!!Note")
