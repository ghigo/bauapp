# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "associazione",
            pkey="codice",
            name_long="Associazione",
            name_plural="Associazioni",
            caption_field="denominazione",lookup=True
        )
        self.sysFields(tbl)
        tbl.column("codice", size=":10", name_long="!!Codice", unique=True)
        tbl.column("denominazione", name_long="denominazione")
        # tbl.column('anagrafica_id',size='22',name_long='Dati anagrafici').relation('anagrafica.id',relation_name='associazione', one_one=True)



 #   def partitionioning_pkeys(self):
 #       if not self.db.currentEnv.get('staff_id'):
 #           return [r['pkey'] for r in self.query(excludeLogicalDeleted=False).fetch()]
 #       else:
 #           return [r['pkey'] for r in self.query(where="$sede_id=:env_sede_id",
 #               excludeLogicalDeleted=False).fetch()] 
