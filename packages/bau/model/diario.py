# encoding: utf-8
import os

class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('diario',pkey='id',name_long='Diario',name_plural='Diari',caption_field='caption_diario')
        self.sysFields(tbl)
        tbl.column('data',dtype='D',name_long='Data',indexed=True)
        tbl.column('ora_inizio',dtype='H',name_long='Inizio',indexed=True)
        tbl.column('ora_fine',dtype='H',name_long='Fine',indexed=True)
        tbl.column('staff_id',size='22',name_long='Staff').relation('staff.id',relation_name='diari', mode='foreignkey')
        tbl.column('cane_id',size='22',name_long='Cane').relation('cane.id',relation_name='diari')
        tbl.column('unita_id',size='22', group='_', name_long=u'!!Unità programma'
                    ).relation('pg_unita.id', relation_name='diari_volontari', mode='foreignkey', onDelete='raise')
        tbl.column('tipo_attivita_id',size='22', group='_', name_long=u'!!Attività'
                    ).relation('tipo_attivita.id', mode='foreignkey', onDelete='raise')
        tbl.column('resoconto',name_long='Resoconto')
        tbl.column('area_id',size='22', group='_', name_long='!!Area'
                    ).relation('pg_area.id', mode='foreignkey', onDelete='raise')
        tbl.column('campi_resoconto',dtype='X',name_long='Campi resoconto',subfields='tipo_attivita_id')
        
        tbl.column('filepath',name_long='!!Filepath',name_short='Filepath')
        tbl.formulaColumn('fileurl',"""CASE WHEN $filepath IS NOT NULL THEN '/_vol/' || $filepath ||'?_lazydoc=bau.diario,' || $id ELSE NULL END""",name_long='Fileurl') 

        tbl.formulaColumn('caption_diario',"@cane_id.nome || '-' || @tipo_attivita_id.descrizione")

    
    def trigger_onInserting(self,record_data):
        record_data['filepath'] = self.getDocumentPath(record_data)

    def trigger_onUpdating(self,record_data,old_record=None):
        record_data['filepath'] = self.getDocumentPath(record_data)
        self.delete_cached_document_pdf(record_data)


    def getDocumentPath(self,record,absolute=False):
        data = record['data']
        filename = record['id']
        result = 'diari/%04i/%02i/%s.pdf' %(data.year,data.month,filename.replace('/','_').replace('.','_'))
        if absolute:
            site = self.db.application.site
            return site.getStaticPath('vol:%s' %result,autocreate=-1)
        return result
        
    def check_cached_document_pdf(self,record):
        path = self.getDocumentPath(record,absolute=True)
        if not os.path.isfile(path):
            self.create_cached_document_pdf(record)

    def create_cached_document_pdf(self,record):
        record= self.recordAs(record)
        page = self.db.currentPage
        if page:
            htmlbuilder = page.loadTableScript(self, 'html_res/diario')
            htmlbuilder(record =record,pdf=True)

    def delete_cached_document_pdf(self,record):
        if record['filepath']:
            path = self.db.application.site.getStaticPath('vol:%s' %record['filepath'],autocreate=-1)
            if os.path.isfile(path):
                os.remove(path)
        #path = self.getDocumentPath(record,absolute=True)
        #if os.path.isfile(path):
        #    os.remove(path)
