# encoding: utf-8

class Table(object):
    def config_db(self,pkg):
        tbl=pkg.table('pensione', pkey='id', name_long='Pensione', 
                    name_plural='Pensioni',caption_field='denominazione')
        self.sysFields(tbl)
        tbl.column('denominazione', size=':40', name_long='Denominazione')
        tbl.column('indirizzo_completo', name_long='Indirizzo')