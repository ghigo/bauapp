# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "vaccino",
            pkey="id",
            name_long="!!Vaccino",
            name_plural="!!Vaccini",
            caption_field="nome",
            lookup=True,
        )
        self.sysFields(tbl, counter=True)
        tbl.column("nome", size=":50", name_long="!!Nome")
        tbl.column("farmaco_id", size="22", group="_", name_long="!!Farmaco").relation(
            "farmaco.id", relation_name="", mode="foreignkey", onDelete="raise"
        )
