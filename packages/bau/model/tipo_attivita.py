# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "tipo_attivita",
            pkey="id",
            name_long=u"Tipo attività",
            name_plural=u"Tipi attività",
            caption_field="descrizione",
        )
        self.sysFields(tbl, hierarchical="descrizione", df=True, counter=True)
        tbl.column("descrizione", size=":40", name_long="!!Titolo")
        tbl.column("annotazioni", name_long="!!Annotazioni")
        tbl.column("richiede_cane", dtype="B", name_long="!!Richiede cane")
        tbl.column("richiede_auto", dtype="B", name_long="!!Richiede auto")
        tbl.column("richiede_diario", dtype="B", name_long="!!Richiede diario")
        tbl.column("default_volontari", dtype="L", name_long="!!Default volontari")
        tbl.column("default_unita", dtype="L", name_long=u"!!Default unità (30min)")
        tbl.column("colore", name_long="!!Colore", name_short="!!Colore")
        tbl.column("template_associato", dtype="X", name_long="!!Template")
