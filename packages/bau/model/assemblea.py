# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "assemblea",
            pkey="id",
            name_long="!!Assemblea",
            name_plural="!!Assemblea",
            caption_field="titolo",
        )
        self.sysFields(tbl)
        tbl.column("titolo", size=":50", name_long="!!Titolo")
        tbl.column("tipo_evento", size=":10", group="_", name_long="!!Tipo").relation(
            "tipo_evento.codice",
            relation_name="eventi",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column("data", dtype="D", name_long="!!Data")
        tbl.column("ora_inizio", dtype="H", name_long="!!Dalle")
        tbl.column("ora_fine", dtype="H", name_long="!!Alle")
        tbl.column("luogo", name_long="!!Luogo")
        tbl.column("verbale", name_long="!!Resoconto")
