# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "banchetto_turno", pkey="id", name_long="!!Turno", name_plural="!!Turni"
        )
        self.sysFields(tbl)
        tbl.column(
            "banchetto_id", size="22", group="_", name_long="!!Banchetto"
        ).relation(
            "banchetto.id", relation_name="turni", mode="foreignkey", onDelete="cascade"
        )
        tbl.column("ora_inizio", dtype="H", name_long="!!Dalle")
        tbl.column("ora_fine", dtype="H", name_long="!!Alle")
        tbl.column("colore", name_long="!!Colore")
        tbl.column("note", name_long="!!Note")

        tbl.aliasColumn("banchetto", "@banchetto_id.nome", name_long="Banchetto")
        tbl.aliasColumn("evento_id", "@banchetto_id.evento_id", name_long="Evento id")

        tbl.formulaColumn(
            "volontari",
            "array_to_string(ARRAY(#volont),', ')",
            select_volont=dict(
                table="bau.banchetto_turno_staff",
                columns="$volontario",
                where="$turno_id=#THIS.id",
            ),
        )

    def trigger_onInserting(self, record):
        if not record["staff_pkeys"]:
            raise self.exception("business_logic", msg="Non hai messo i volontari")
        if record["staff_pkeys"]:
            self.triggerStaff(record)

    def trigger_onUpdating(self, record, old_record=None):
        self.triggerStaff(record)

    def triggerStaff(self, record):
        tblturnostaff = self.db.table("bau.banchetto_turno_staff")
        turno_id = record["id"]
        tblturnostaff.sql_deleteSelection(where="$turno_id=:pk", pk=turno_id)
        if record["staff_pkeys"]:
            for staff_id in record["staff_pkeys"].split(","):
                tblturnostaff.insert(dict(staff_id=staff_id, turno_id=turno_id))
