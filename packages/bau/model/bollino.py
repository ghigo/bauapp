# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "bollino",
            pkey="codice",
            name_long="Bollino",
            name_plural="Bollini",
            caption_field="nome",
            lookup=True,
        )
        self.sysFields(tbl, id=False, counter=True)
        tbl.column("codice", size=":10", name_long="Codice", unique=True, indexed=True)
        tbl.column("nome", size=":30", name_long="Nome")
        tbl.column("colore", name_long="Colore", cell_edit=dict(tag="colorTextBox"))
        tbl.column("annotazioni", name_long="!!Note")
