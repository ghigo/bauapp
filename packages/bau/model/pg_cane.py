# encoding: utf-8


class Table(object):
    def config_db(self, pkg):
        tbl = pkg.table(
            "pg_cane",
            pkey="id",
            name_long="!!Programma cane",
            name_plural="!!Programmi cani",
        )
        self.sysFields(tbl)
        tbl.column("cane_id", size="22", group="_", name_long="!!Cane").relation(
            "cane.id",
            relation_name="cane_programmi",
            mode="foreignkey",
            onDelete="raise",
        )
        tbl.column(
            "programma_id", size="22", group="_", name_long="!!Programma"
        ).relation(
            "pg_programma.id",
            relation_name="cani_coinvolti",
            mode="foreignkey",
            onDelete="cascade",
        )
