from gnr.web.gnrbaseclasses import BaseComponent


class LoginComponent(BaseComponent):
    def onUserSelected(self, avatar, data):
        staff_id = self.db.table("adm.user").readColumns(
            pkey=avatar.user_id, columns="@baustaff.id"
        )
        data.setItem("staff_id", staff_id)
        if avatar.group_code == "vol" or avatar.group_code == "edu":
            data["custom_index"] = "volontario"
            return
