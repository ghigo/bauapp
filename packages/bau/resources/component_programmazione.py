# encoding: utf-8

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method
from gnr.web.gnrwebstruct import struct_method
from gnr.core.gnrbag import Bag


class ProgrammazioneManager(BaseComponent):
    py_requires = """th/th:TableHandler,gnrcomponents/timesheet_viewer/timesheet_viewer:TimesheetViewer"""
    # js_requires='appuntamenti'
    css_requires = "bau"

    @struct_method
    def ap_programmazioneManager(self, parent, **kwargs):
        parent.dataRpc(
            ".programmazione",
            self.getAttivitaStaff,
            programma_id="=#FORM.record.id",
            data_programma="=#FORM.record.data",
            _if="data_programma && programma_id",
            _fired="^.refresh",
        )
        form_unita = self.formUnita(parent).js_form
        parent.dataController(
            """frm.load({destPkey:unita_id})""",
            frm=form_unita,
            subscribe_modifica_unita=True,
        )
        parent.dataController(
            """
        frm.newrecord({programma_id:programma_id,staff_id:staff_id})
        """,
            frm=form_unita,
            subscribe_nuova_attivita=True,
        )

        frame = parent.timesheetViewer(
            region="center",
            value="^.programmazione",
            selfsubscribe_edit_timesheet="""
                                PUBLISH nuova_attivita = {programma_id:$1.programma_id,staff_id:$1.staff_id};
                                """,
            minute_height=5,
            selected_date="=#FORM.record.data",
            selfsubscribe_edit_slot="PUBLISH modifica_unita = {unita_id:$1.unita_id}",
            slotFiller="timetable",
            work_start="=#FORM.record.ora_inizio?=#v?(#v.getHours()):13",
            work_end="=#FORM.record.ora_fine?=#v?(#v.getHours()+1):19",
            viewerMode="singleDay",
            slot_duration=30,
        )
        parent.onDbChanges(
            """FIRE .refresh""",
            table="bau.pg_programma",
            programma_id="^#FORM.controller.loaded",
        )
        parent.onDbChanges("""FIRE .refresh""", table="bau.pg_unita")

    def formUnita(self, parent):
        return parent.thFormHandler(
            table="bau.pg_unita",
            formResource="FormFromProgramma",
            modal=True,
            dialog_height="400px",
            dialog_width="600px",
            parentForm=False,
            formId="unita_programma",
            datapath="#FORM.unita_programma",
        )

    @public_method
    def getAttivitaStaff(self, programma_id=None, data_programma=None):
        result = Bag()

        programma_volontario = (
            self.db.table("bau.pg_staff")
            .query(
                where="$programma_id=:pid",
                pid=programma_id,
                columns="""$staff_id,$volontario,
                                                    $ora_inizio_calc AS ora_inizio,$ora_fine_calc AS ora_fine""",
                order_by="$_row_count",
            )
            .fetchAsDict("volontario", ordered=True)
        )

        attivita_grouped = (
            self.db.table("bau.pg_unita_staff")
            .query(
                where="$programma_id=:pid",
                columns="""$volontario,$unita_id,
                                                    $ora_inizio,$ora_fine,
                                                    $volontari_full,$cane,$area,$unita_descrizione,
                                                    $colore_unita""",
                pid=programma_id,
                order_by="$ora_inizio",
            )
            .fetchGrouped("volontario")
        )
        tpl = """<div class='cls_vol'>$volontari_full</div>
                <div class='cls_cane'>${Con: <b>$cane</b>}</div>
                <div class='cls_area'>${Dove: $area}</div>
                <div class='cls_attivita'>$unita_descrizione</div>
                """
        channels = list(programma_volontario.keys())
        calcontent = Bag()
        daylabel = self.toText(data_programma, format="yyyy_MM_dd")
        programma = result.setItem(daylabel, calcontent, day=data_programma)

        for volontario in channels:
            attivita_volontario = attivita_grouped.get(volontario, [])
            val = Bag()
            prog_vol = programma_volontario[volontario]
            calcontent.setItem(
                volontario,
                val,
                time_start=prog_vol["ora_inizio"],
                time_end=prog_vol["ora_fine"],
                programma_id=programma_id,
                staff_id=prog_vol["staff_id"],
            )
            for r in attivita_volontario:
                val.setItem(
                    self.toText(r["ora_inizio"], format="HH_mm"),
                    None,
                    time_start=r["ora_inizio"],
                    time_end=r["ora_fine"],
                    unita_id=r["unita_id"],
                    volontari_full=r["volontari_full"],
                    cane=r["cane"],
                    area=r["area"],
                    unita_descrizione=r["unita_descrizione"],
                    background_color=r["colore_unita"],
                    template=tpl,
                )
        return result, dict(channels=channels)
