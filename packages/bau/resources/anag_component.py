#!/usr/bin/env python
# encoding: utf-8

from gnr.web.gnrwebpage import BaseComponent
from gnr.web.gnrwebstruct import struct_method
from gnr.core.gnrdecorator import extract_kwargs, public_method


class AnagraficaComponent(BaseComponent):
    js_requires = "bau"
    css_requires = "bau"

    @extract_kwargs(fb=True)
    @struct_method
    def anagrafica_anagraficaPane(
        self,
        pane,
        tipo_anagrafica=None,
        fb_kwargs=None,
        linkerBar=True,
        datapath=".@anagrafica_id",
        excludeList=None,
        fldExtraKwargs=None,
        title=None,
        openIfEmpty=None,
        saveIndirizzoEsteso=True,
        **kwargs
    ):
        frame = pane.roundedGroupFrame(datapath=datapath, title=title, **kwargs)
        if linkerBar:
            frame.top.linkerBar(
                field="anagrafica_id",
                label=title or "!![it]Dati anagrafici",
                table="bau.anagrafica",
                newRecordOnly=linkerBar == "newRecordOnly",
                openIfEmpty=openIfEmpty,
            )
        fbkw = dict(
            cols=2,
            border_spacing="4px",
            fld_width="100%",
            width="100%",
            colswidth="auto",
            dbtable="bau.anagrafica",
            fld_html_label=True,
            lbl_margin_left="10px",
        )
        fbkw.update(fb_kwargs)
        fb = frame.div(margin_top="5px", margin_right="20px").formbuilder(**fbkw)
        fb._excludedFieldList = excludeList or []
        fb._fldExtraKwargs = fldExtraKwargs or []

        fb.controllerAnagrafica(tipo_anagrafica=tipo_anagrafica)
        if not tipo_anagrafica:
            fb.fieldAnagrafica("societa", lbl_width="9em", lbl=u"!![it]Società")
            fb.fieldAnagrafica(
                "titolo",
                lbl="!![it]Titolo",
                tag="combobox",
                values="Sig,Sign.ra,Dott.,Ing.",
                width="10em",
            )
            fb.fieldAnagrafica("nome", row_hidden="^.societa")
            fb.fieldAnagrafica("cognome")
            fb.fieldAnagrafica(
                "ragione_sociale",
                row_hidden="^.societa?=!#v",
                lbl="!![it]Ragione sociale",
                colspan=fbkw.get("cols"),
            )

        if tipo_anagrafica == "persona":
            fb.br()
            cols = fbkw.get("cols")
            fb.fieldAnagrafica(
                "titolo",
                lbl="!![it]Titolo",
                tag="combobox",
                values="Sig,Sign.ra,Dott.,Ing.",
                width="6em",
            )
            if cols != 3:
                fb.br()
            fb.fieldAnagrafica("nome")
            fb.fieldAnagrafica("cognome")
        if tipo_anagrafica == "societa":
            fb.br()
            fb.fieldAnagrafica(
                "ragione_sociale", lbl="!![it]Ragione sociale", colspan=fbkw.get("cols")
            )

        if saveIndirizzoEsteso:
            fb.fieldAnagrafica(
                "indirizzo_esteso",
                tag="geoCoderField",
                colspan=fbkw.get("cols"),
                lbl="Indirizzo esteso",
                selected_street_address=".indirizzo",
                selected_locality=".localita",
                selected_postal_code=".cap",
                selected_administrative_area_level_2=".$area_level_2",
                selected_country=".nazione",
                selected_position=".geocoords",
                country="=.nazione",
                ghost=u"Strada, Numero, Località",
            )
        else:
            if "indirizzo_esteso" not in fb._excludedFieldList:
                fb.geoCoderField(
                    "^.$indirizzo_esteso",
                    colspan=fbkw.get("cols"),
                    lbl="Cerca Indirizzo",
                    selected_street_address=".indirizzo",
                    selected_locality=".localita",
                    selected_postal_code=".cap",
                    selected_administrative_area_level_2=".$area_level_2",
                    selected_country=".nazione",
                    selected_position=".geocoords",
                    country="=.nazione",
                    ghost=u"Strada, Numero, Località",
                )
        fb.dataFormula(
            ".provincia",
            "provincia",
            provincia="^.$area_level_2",
            nazione="=.nazione",
            _if='nazione=="IT"',
            _else="null",
            _userChanges=True,
        )

        fb.fieldAnagrafica("indirizzo", colspan=fbkw.get("cols"))
        fb.fieldAnagrafica("provincia")
        fb.fieldAnagrafica(
            "localita",
            tag="dbCombobox",
            dbtable="glbl.localita",
            auxColumns="$provincia",
            selected_provincia=".provincia",
            selected_cap=".cap",
            rowcaption="nome",
        )
        fb.fieldAnagrafica("cap", max_width="10em")
        fb.fieldAnagrafica("nazione", default_value="IT")
        fb.fieldAnagrafica("telefono")
        fb.fieldAnagrafica("cellulare")
        fb.fieldAnagrafica("email", colspan=2, validate_email=True)
        fb.fieldAnagrafica("fax")
        fb.fieldAnagrafica("voip")
        fb.fieldAnagrafica("chat")
        fb.fieldAnagrafica("www")

        # fb.field('telefono_mv',tag='div')
        if tipo_anagrafica == "persona":
            fb.fieldAnagrafica(
                "sesso", tag="filteringSelect", values="M:Maschio,F:Femmina"
            )
            fb.fieldAnagrafica("data_nascita", popup=False)
            fb.fieldAnagrafica("luogo_nascita")

        fb.fieldAnagrafica("codice_fiscale")
        fb.fieldAnagrafica("partita_iva")
        frame._fb = fb
        return frame

    @struct_method
    def anag_fieldAnagrafica(self, fb, field_name, **kwargs):
        if field_name not in fb._excludedFieldList:
            if field_name in fb._fldExtraKwargs:
                kwargs.update(fb._fldExtraKwargs[field_name])
            fb.field(field_name, **kwargs)

    @struct_method
    def anagrafica_controllerAnagrafica(self, pane, tipo_anagrafica=None):
        pane.data("#FORM.anagrafica.upCityname", "")
        pane.dataFormula(
            "#FORM.anagrafica.upCityname",
            "city.toUpperCase();",
            city="^.localita",
            _if="city",
        )
        pane.dataController(
            """
                            if (cognome &&  nome){
                                SET .ragione_sociale=cognome+' '+nome;
                            }else{
                                 SET .ragione_sociale=cognome;
                            }
                            """,
            cognome="^.cognome",
            nome="^.nome",
            _if="cognome",
        )

        if tipo_anagrafica:
            pane.dataController(
                "SET .societa = tipo_anagrafica=='societa';",
                _fired="^.id",
                tipo_anagrafica=tipo_anagrafica,
            )
        pane.dataController(
            "SET .denominazione_cortesia = ragsoc;",
            ragsoc="^.ragione_sociale",
            societa="^.societa",
            _if="!denominazione_cortesia&&societa",
            denominazione_cortesia="=.denominazione_cortesia",
        )
        pane.dataController(
            "SET .denominazione_cortesia = (nome||'')+' '+(cognome||'');",
            cognome="^.cognome",
            nome="^.nome",
            societa="^.societa",
            _if="!denominazione_cortesia&&!societa&&(nome||cognome)",
            denominazione_cortesia="=.denominazione_cortesia",
        )
