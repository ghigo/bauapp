#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("programma_id")
        r.fieldcell("cane_id")
        r.fieldcell("tipo_attivita_id")
        r.fieldcell("descrizione")
        r.fieldcell("ora_inizio")
        r.fieldcell("ora_fine")

    def th_order(self):
        return "programma_id"

    def th_query(self):
        return dict(column="programma_id", op="contains", val="")


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("programma_id")
        fb.field("cane_id")
        fb.field("tipo_attivita_id")
        fb.field("descrizione")
        fb.field("ora_inizio")
        fb.field("ora_fine")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
