#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("nome", width="10em")
        r.fieldcell("ragione_sociale", width="20em")
        r.fieldcell("gdpr", width="5em")
        r.fieldcell("email", width="20em")
        r.fieldcell("data_iscrizione")
        r.fieldcell("user_id")
        r.fieldcell("livello_codice")
        r.fieldcell("cani", width="20em")

    def th_order(self):
        return "nome"

    def th_query(self):
        return dict(column="nome", op="contains", val="")

    def th_queryBySample(self):
        return dict(
            fields=[
                dict(field="nome", lbl="Staff", width="10em"),
                dict(field="cani", lbl="Cani collegati", width="15em"),
                dict(
                    field="livello_codice",
                    lbl="Livello",
                    width="10em",
                    tag="dbSelect",
                    dbtable="bau.bollino",
                    hasDownArrow=True,
                ),
            ],
            cols=3,
            isDefault=True,
        )

    # def th_options(self):
    #    return dict(virtualStore=False,extendedQuery=False)


class ViewPicker(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("nome", width="20em")
        r.fieldcell("livello_codice", width="10em")
        r.fieldcell("@livello_codice.bollino_max", width="10em", name="Bollino")
        r.fieldcell("data_iscrizione", width="7em")

    def th_order(self):
        return "nome"


class Form(BaseComponent):
    py_requires = "anag_component:AnagraficaComponent,gnrcomponents/attachmanager/attachmanager:AttachManager"

    def th_form(self, form):
        bc = form.center.borderContainer()
        form.dataRpc(
            None,
            self.creaUtente,
            _fired="^#FORM.creaUtente",
            record_id="=#FORM.record.id",
            _if="record_id",
            _onResult="this.form.reload()",
        )
        self.staff_top(
            bc.borderContainer(region="top", height="180px", datapath=".record")
        )

        tc = bc.tabContainer(region="center", margin="2px")
        bc = tc.borderContainer(
            title="Dati anagrafici e annotazioni", datapath=".record"
        )
        bc.contentPane(region="center").anagraficaPane(
            tipo_anagrafica="persona",
            saveIndirizzoEsteso=False,
            excludeList=[
                "fax",
                "voip",
                "www",
                "chat",
                "rea_provincia",
                "rea",
                "partita_iva",
            ],
            linkerBar=False,
        )
        bc.roundedGroupFrame(
            title="Note", overflow="hidden", region="right", width="300px"
        ).center.simpleTextArea(value="^.note")
        tc.contentPane(title="!![it]Allegati").attachmentMultiButtonFrame()

    def staff_top(self, bc):
        center = bc.borderContainer(region="center", margin="4px")
        center.contentPane(region="right").img(
            src="^#FORM.record.foto",
            crop_height="110px",
            crop_width="110px",
            crop_margin_left="20px",
            crop_border="2px dotted silver",
            crop_rounded=6,
            edit=True,
            placeholder=True,
            upload_folder="vol:avatar/staff",
            upload_filename="=#FORM.record.id",
            colspan=1,
            rowspan=4,
        )
        fb = (
            center.contentPane(region="center")
            .div(margin_right="10px")
            .formbuilder(cols=2, colswidth="auto", width="100%", fld_width="100%")
        )
        fb.field("nome", max_width="20em")
        fb.field("data_richiesta", max_width="8em")
        fb.field("associazione_codice", max_width="20em", hasDownArrow=True)
        fb.field("data_accettazione", max_width="8em")
        fb.field("livello_codice", width="20em")
        fb.field("data_iscrizione", max_width="8em")
        fb.field("gdpr", html_label=True)
        fb.field("tessera_numero", width="8em")
        fb.field("quota_versata", html_label=True)
        fb.field("tipo_socio", width="10em")
        fb.field("volontario", html_label=True)
        fb.button("Crea utente", hidden="^.user_id", fire="#FORM.creaUtente")

        bc.contentPane(region="right", width="300px").linkerBox(
            "user_id",
            label="Informazioni Utente",
            formUrl="/adm/user_page",
            dialog_height="400px",
            dialog_width="650px",
            default_firstname="=#FORM.record.@anagrafica_id.nome",
            default_lastname="=#FORM.record.@anagrafica_id.cognome",
            default_email="=#FORM.record.@anagrafica_id.email",
            newRecordOnly=False,
            margin="2",
        )

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")

    @public_method
    def creaUtente(self, record_id=None, **kwargs):
        with self.db.table("bau.staff").recordToUpdate(record_id) as rec:
            self.db.table("bau.staff").createNewUser(rec)
        self.db.commit()


class PaginaVolontario(BaseComponent):
    py_requires = "public:Public,anag_component:AnagraficaComponent,gnrcomponents/attachmanager/attachmanager:AttachManager"

    def th_form(self, form):
        bc = form.center.borderContainer(font_size="1.1em")
        bc.contentPane(
            region="top", height="50px", background="RGBA(255, 253, 123, 0.10)"
        ).div(
            template="Ciao $nome",
            datasource="^#FORM.record",
            font_size="14pt",
            font_weight="bold",
            color="#444",
            padding="5px",
        )
        tc = bc.tabContainer(margin="2px", region="center")
        # self.ultimeAttivita(tc.contentPane(title=u'Attività in canile'))
        self.volontarioDiari(tc.contentPane(title="Diari"))
        tc.contentPane(title="Cani").remote(self.volontarioCani)
        tc.contentPane(title="Prossimi eventi").remote(self.prossimiEventi)

        tc.contentPane(title="Dati personali", datapath=".record").anagraficaPane(
            tipo_anagrafica="persona",
            saveIndirizzoEsteso=False,
            excludeList=[
                "fax",
                "voip",
                "www",
                "chat",
                "rea_provincia",
                "rea",
                "partita_iva",
            ],
            linkerBar=False,
        )

    def ultimeAttivita(self, pane):
        pass

    def volontarioDiari(self, pane):
        pane.dialogTableHandler(
            relation="@diari",
            formResource="FormFromVolontario",
            viewResource="ViewFromVolontario",
            view_store__onStart=True,
            configurable=False,
            pbl_classes=True,
            margin="2px",
        )

    @public_method
    def prossimiEventi(self, pane):
        pane.plainTableHandler(
            table="bau.evento",
            viewResource="ViewFromVolontario",
            condition="$mostra IS TRUE AND $data>=:env_workdate",
            view_store__onBuilt=True,
            pbl_classes=True,
            margin="2px",
            addrow=False,
            delrow=False,
        )
        form = pane.thFormHandler(
            table="bau.evento_staff",
            formResource="FormFromVolontario",
            default_staff_id=self.rootenv["staff_id"],
            dialog_height="200px",
            dialog_width="400px",
            modal=True,
        )
        pane.dataController(
            """
        if(disponibilita_id){
            frm.load({destPkey:disponibilita_id});
        }else{
            frm.newrecord({evento_id:evento_id});
        }
        """,
            subscribe_edit_disponibilita=True,
            frm=form.js_form,
        )

    @public_method
    def volontarioCani(self, pane):
        pane.dialogTableHandler(
            table="bau.cane",
            formResource="FormFromVolontario",
            view_store__onBuilt=True,
            pbl_classes=True,
            margin="2px",
            addrow=False,
            delrow=False,
            configurable=False,
            dialog_windowRatio=0.95,
            form_modal=True,
            formInIframe=True,
        )

    def th_options(self):
        return dict(showtoolbar=False, autoSave=True)
