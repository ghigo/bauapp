#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data")
        r.fieldcell("cane_id")
        r.fieldcell("vaccino_id")
        r.fieldcell("note")

    def th_order(self):
        return "data"

    def th_query(self):
        return dict(column="data", op="contains", val="")


class ViewFromCane(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("vaccino_id", width="20em")
        r.fieldcell("data", width="8em", edit=True)
        r.fieldcell("note", edit=True, width="100%")

    def th_order(self):
        return "data"


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("data")
        fb.field("cane_id")
        fb.field("vaccino_id")
        fb.field("note")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
