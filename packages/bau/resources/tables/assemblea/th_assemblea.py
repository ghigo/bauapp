#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data")
        r.fieldcell("titolo")
        r.fieldcell("ora_inizio")
        r.fieldcell("ora_fine")
        r.fieldcell("luogo")

    def th_order(self):
        return "data"

    def th_query(self):
        return dict(column="titolo", op="contains", val="")


class Form(BaseComponent):
    py_requires = "gnrcomponents/attachmanager/attachmanager:AttachManager"

    def th_form(self, form):
        bc = form.center.borderContainer()
        fb = bc.contentPane(region="top", datapath=".record").formbuilder(
            cols=3, border_spacing="4px"
        )
        fb.field("data")
        fb.field("titolo", colspan=2, width="100%")
        fb.field("ora_inizio")
        fb.field("ora_fine")
        fb.field("luogo")
        tc = bc.tabContainer(margin="2px", region="center")
        tc.contentPane(title="Presenti").plainTableHandler(
            relation="@partecipanti",
            picker="staff_id",
            addrow=False,
            pbl_classes=True,
            margin="2px",
        )
        tc.contentPane(title="Verbale", overflow="hidden").ckeditor(
            value="^.verbale", toolbar="simple"
        )
        tc.contentPane(title="!![it]Allegati").attachmentMultiButtonFrame()

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
