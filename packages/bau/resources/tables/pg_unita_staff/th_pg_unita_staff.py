#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data", width="8em")
        r.fieldcell("staff_id", width="15em", name="Volontario")
        r.fieldcell("cane", width="10em")
        r.fieldcell("attivita", name=u"Attività")

    def th_order(self):
        return "data"

    def th_options(self):
        return dict(thwidget="plain")

    def th_condition(self):
        return dict(condition="$cane IS NOT NULL")

    def th_queryBySample(self):
        return dict(
            fields=[
                dict(field="data", lbl="Data", width="10em"),
                dict(field="@staff_id.nome", lbl="Volontari", width="20em"),
                dict(field="cane", lbl="Cani", width="20em"),
                dict(field="attivita", lbl=u"Attività", width="20em"),
            ],
            cols=4,
            isDefault=True,
        )


class ViewFromUnita(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("_row_count", counter=True, hidden=True)
        r.fieldcell(
            "staff_id",
            edit=dict(
                condition="$id IN :staff_disponibili",
                # weakCondition="$id NOT IN $staff_indisponibili",
                validate_gridnodup=True,
                condition_staff_disponibili="=#FORM.record.$staff_disponibili",
                # condition_staff_indisponibili='=#FORM.record.$staff_indisponibili',
                hasDownArrow=True,
                auxColumns="$livello_codice,$bollino",
            ),
            width="20em",
            name="Volontario",
        )
        r.checkboxcolumn("responsabile", name="Resp.", width="5em")
        r.checkboxcolumn("automobile", name="Auto.", width="5em")
        r.fieldcell("note", edit=True, width="100%")

    def th_order(self):
        return "_row_count"


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("staff_id")
        fb.field("unita_id")
        fb.field("responsabile")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
