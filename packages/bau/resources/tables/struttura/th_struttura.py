#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("hierarchical_nome")

    def th_order(self):
        return "hierarchical_nome"

    def th_query(self):
        return dict(column="parent_id", op="contains", val="")


class Form(BaseComponent):
    def th_form(self, form):
        bc = form.center.borderContainer()
        fb = bc.contentPane(region="top", datapath=".record").formbuilder(
            cols=1, border_spacing="4px"
        )
        fb.field("nome", width="20em")
        fb.field("descrizione", width="20em", tag="simpleTextArea")
        fb.field("n_cani_entro_10", width="6em")
        fb.field("n_cani_10_25", width="6em")
        fb.field("n_cani_25_oltre", width="6em")
        th = bc.contentPane(region="center").plainTableHandler(
            relation="@cani", pbl_classes=True, margin_left="6px", margin="2px"
        )
        form.htree.relatedTableHandler(th, inherited=True)

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px", hierarchical=True)
