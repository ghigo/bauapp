#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("ragione_sociale")
        r.fieldcell("cognome")
        r.fieldcell("nome")
        r.fieldcell("indirizzo_esteso")
        r.fieldcell("geocoords")
        r.fieldcell("societa")
        r.fieldcell("indirizzo")
        r.fieldcell("numero_civico")
        r.fieldcell("cap")
        r.fieldcell("localita")
        r.fieldcell("provincia")
        r.fieldcell("comune_id")
        r.fieldcell("nazione")
        r.fieldcell("titolo")
        r.fieldcell("codice_fiscale")
        r.fieldcell("partita_iva")
        r.fieldcell("rea")
        r.fieldcell("rea_provincia")
        r.fieldcell("data_nascita")
        r.fieldcell("luogo_nascita")
        r.fieldcell("provincia_nascita")
        r.fieldcell("sesso")
        r.fieldcell("note")
        r.fieldcell("telefono")
        r.fieldcell("cellulare")
        r.fieldcell("fax")
        r.fieldcell("chat")
        r.fieldcell("email")

    def th_order(self):
        return "ragione_sociale"

    def th_query(self):
        return dict(column="ragione_sociale", op="contains", val="")


class Form(BaseComponent):
    py_requires ='anag_component'
    def th_form(self, form):
        form.record.anagraficaPane(
            partita_iva_obbligatoria=True,
            saveIndirizzoEsteso=False,
            datapath=".record",
            fldExtraKwargs={
                "nazione": dict(validate_notnull=True),
                "codice_fiscale": dict(validate_notnull="^.societa?=!#v"),
                "indirizzo": dict(validate_notnull=True),
                "provincia": dict(validate_notnull=True),
                "localita": dict(validate_notnull=True),
                "cap": dict(validate_notnull=True),
                "email": dict(validate_notnull=True),
            },
            excludeList=[
                "fax",
                "voip",
                "www",
                "chat",
                "rea_provincia",
                "rea",
                "partita_iva",
            ],
            linkerBar=False,
        )

    def th_options(self):
        return dict(dialog_height="320px", dialog_width="600px")
