#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("titolo")

    def th_order(self):
        return "titolo"

    def th_query(self):
        return dict(column="titolo", op="contains", val="")

    def th_options(self):
        return dict(virtualStore=False, extendedQuery=False)


class Form(BaseComponent):
    py_requires = "gnrcomponents/dynamicform/dynamicform:DynamicForm"

    def th_form(self, form):
        bc = form.center.borderContainer()
        fb = bc.contentPane(region="top", datapath=".record").formbuilder(
            cols=2, border_spacing="4px"
        )
        fb.field("titolo")
        bc.contentPane(region="center").fieldsGrid(
            margin="2px", rounded=6, border="1px solid silver"
        )

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
