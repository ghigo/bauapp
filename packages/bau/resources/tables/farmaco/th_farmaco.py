#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("descrizione")
        r.fieldcell("farmaco_tipo_id")
        r.fieldcell("formato")
        r.fieldcell("prescrizione")

    def th_order(self):
        return "descrizione"

    def th_query(self):
        return dict(column="descrizione", op="contains", val="")


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("descrizione")
        fb.field("farmaco_tipo_id")
        fb.field("formato")
        fb.field("prescrizione")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
