#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("evento_id")
        r.fieldcell("staff_id")
        r.fieldcell("ora_inizio")
        r.fieldcell("ora_fine")

    def th_order(self):
        return "evento_id"

    def th_query(self):
        return dict(column="evento_id", op="contains", val="")


class ViewFromEvento(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("staff_id", width="15em")
        r.fieldcell(
            "ora_inizio",
            _customGetter="""function(row){
            return row.ora_inizio || this.grid.sourceNode.getRelativeData('#FORM.record.ora_inizio');
        }""",
            edit=True,
            width="8em",
        )
        r.fieldcell(
            "ora_fine",
            _customGetter="""function(row){
            return row.ora_fine || this.grid.sourceNode.getRelativeData('#FORM.record.ora_fine');
        }""",
            edit=True,
            width="8em",
        )
        r.fieldcell("note", edit=True, width="100%")


class FormFromVolontario(BaseComponent):
    def th_form(self, form):
        pane = form.record.div(padding="10px")
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field(
            "note",
            tag="simpleTextArea",
            lbl="Dettagli partecipazione",
            width="100%",
            colspan=2,
            height="120px",
        )

        fb.field("ora_inizio", width="7em", lbl="Dalle")
        fb.field("ora_fine", width="7em", lbl="Alle")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px", modal=True)


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("evento_id")
        fb.field("staff_id")
        fb.field("ora_inizio")
        fb.field("ora_fine")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
