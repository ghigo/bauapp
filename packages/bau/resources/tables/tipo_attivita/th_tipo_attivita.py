#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("hierarchical_descrizione")

    def th_order(self):
        return "hierarchical_descrizione"

    def th_query(self):
        return dict(column="hierarchical_descrizione", op="contains", val="")


class Form(BaseComponent):
    py_requires = "gnrcomponents/dynamicform/dynamicform:DynamicForm"

    def th_form(self, form):
        bc = form.center.borderContainer()
        fb = (
            bc.contentPane(region="top", datapath=".record")
            .div(max_width="600px", margin_right="20px")
            .formbuilder(cols=3, border_spacing="4px", colswidth="auto", width="100%")
        )
        fb.field("descrizione", colspan=3, width="100%")
        fb.field("default_volontari", width="5em")
        fb.field("default_unita", width="5em")
        fb.field("colore", tag="colorTextBox", mode="rgba")
        fb.field("richiede_cane", html_label=True)
        fb.field("richiede_diario", html_label=True)
        fb.field("richiede_auto", html_label=True)
        fb.field(
            "annotazioni", colspan=3, width="100%", tag="SimpleTextArea", height="90px"
        )
        rpc = fb.dataRecord(
            "#FORM.diario_esempio.record",
            "bau.diario",
            pkey='==_pkey || "*newrecord*"',
            _pkey="^#FORM.diario_esempio.pkey",
            default_tipo_attivita_id="^#FORM.pkey",
            ignoreMissing=True,
        )

        tc = bc.tabContainer(region="center")
        tc.contentPane(title="Campi").fieldsGrid(
            margin="2px", rounded=6, border="1px solid silver"
        )
        paper = tc.contentPane(title="Template").div(
            height="290mm",
            width="210mm",
            margin="10px",
            border="1px dotted silver",
            datapath="#FORM.record",
        )
        paper.templateChunk(
            template="^.template_associato",
            table="bau.diario",
            editable=True,
            dataProvider=rpc,
            datasource="^#FORM.diario_esempio.record",
            selfsubscribe_onChunkEdit="this.form.save();",
        )

    def th_options(self):
        return dict(
            dialog_height="400px",
            dialog_width="600px",
            hierarchical=True,
            duplicate=True,
        )
