#!/usr/bin/env python
# encoding: utf-8

from gnr.web.gnrbaseclasses import TableScriptToHtml

CURRENCY_FORMAT = "#,###.00"


class Main(TableScriptToHtml):
    maintable = "bau.tipo_attivita"

    def main(self):
        page = self.getNewPage()
        principale = page.layout(
            "principale", top=1, left=1, right=1, bottom=1, border_width=0
        )
        self.testata(principale.row(height=10))
        self.righe(principale.row().cell())
        self.body.style(
            """.sezione_label{
                font-size:13pt; 
                font-weight:bold;
                text-align:left;
                color:#444;
                padding-left:5mm;
                margin-top:5mm;
            }
            .sotto_sezione{
                font-size:12pt; 
                color:#444;
                margin-top:1mm;
            }

            """
        )
        page = self.getNewPage()
        page.div(
            "Resoconto libero",
            style="font-size:16pt; font-weight:bold;color:#444; text-align:center;",
        )

    def testata(self, row):
        row.cell("", lbl="Volontario")
        row.cell("", lbl="Cane")
        row.cell(self.field("descrizione"), lbl="Attività")
        row.cell("", lbl="Luogo")
        row.cell("", lbl="Data")

    def righe(self, page):
        subfields = self.db.table("bau.tipo_attivita").df_subFieldsBag(
            self.record["id"]
        )
        for pagename, content in subfields.items():
            codici_attivi = content.keys()
            if not codici_attivi:
                continue
            page.div(pagename, _class="sezione_label")
            box = page.div(
                style="border:1px solid silver; margin:1mm;border-radius:2mm;padding:2mm;"
            )

            for c in codici_attivi:
                n = content.getNode(c)
                box.div("<b>%(caption)s:</b>::HTML" % n.attr, _class="sotto_sezione")

    def outputDocName(self, ext=""):
        n = self.getData("record.id")
        return "%s.%s" % (n.replace(".", "_").replace("/", "_"), ext)
