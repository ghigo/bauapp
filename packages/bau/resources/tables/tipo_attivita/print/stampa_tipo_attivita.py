# -*- coding: UTF-8 -*-

# test_special_action.py
# Created by Francesco Porcari on 2010-07-02.
# Copyright (c) 2010 Softwell. All rights reserved.

from gnr.web.batch.btcprint import BaseResourcePrint

caption = u"Stampa attività"
tags = "user"
description = u"Stampa attività"


class Main(BaseResourcePrint):
    batch_prefix = "diario"
    batch_title = u"Stampa attività"
    batch_cancellable = True
    batch_immediate = "print"
    batch_delay = 0.5
    html_res = "html_res/tipo_attivita_paper"
