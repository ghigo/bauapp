#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class ViewProgrammaPappe(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("_row_count", counter=True, width="4em", name="Ord.")
        r.fieldcell("struttura_id", width="20em")
        r.fieldcell("cane_id", width="20em")
        r.fieldcell("razione", width="30em", edit=dict(tag="quickEditor"))
        r.fieldcell("terapia", width="20em", edit=dict(tag="quickEditor"))

    def th_order(self):
        return "_row_count"


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("programma_id")
        fb.field("cane_id")
        fb.field("razione")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
