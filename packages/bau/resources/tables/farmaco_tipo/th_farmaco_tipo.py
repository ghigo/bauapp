#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("_h_count", hidden=True)
        r.fieldcell("hierarchical_descrizione")

    def th_order(self):
        return "_h_count"

    def th_query(self):
        return dict(column="hierarchical_descrizione", op="contains", val="")


class Form(BaseComponent):
    def th_form(self, form):
        bc = form.center.borderContainer()
        fb = bc.contentPane(region="top", datapath=".record").formbuilder(
            cols=2, border_spacing="4px"
        )
        fb.field("categoria", hidden="^.parent_id")
        fb.field("descrizione")
        th = bc.contentPane(region="center").dialogTableHandler(
            relation="@farmaci", pbl_classes=True, margin_left="6px", margin="2px"
        )
        form.htree.relatedTableHandler(th, inherited=True)

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px", hierarchical=True)
