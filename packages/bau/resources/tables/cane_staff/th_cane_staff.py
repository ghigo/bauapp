#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("_row_count")
        r.fieldcell("staff_id")
        r.fieldcell("cane_id")
        r.fieldcell("ruolo")

    def th_order(self):
        return "_row_count"

    def th_query(self):
        return dict(column="_row_count", op="contains", val="")


class ViewFromCane(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("_row_count", counter=True, hidden=True)
        r.fieldcell("staff_id", name="Volontario", width="20em")
        r.fieldcell("ruolo", edit=True, width="10em")
        r.fieldcell("annotazioni", edit=True, width="20em")

    def th_order(self):
        return "_row_count"


class ViewFromStaff(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("cane_id", name="Cane", width="20em")
        r.fieldcell("ruolo", edit=True, width="10em")
        r.fieldcell("annotazioni", edit=True, width="20em")

    def th_order(self):
        return "cane_id"


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("_row_count")
        fb.field("staff_id")
        fb.field("cane_id")
        fb.field("ruolo")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
