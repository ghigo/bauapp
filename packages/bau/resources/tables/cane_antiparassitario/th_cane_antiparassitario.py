#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class ViewFromCane(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("farmaco_id", width="20em")
        r.fieldcell("data", width="8em", edit=True)
        r.fieldcell("note", edit=True, width="100%")

    def th_order(self):
        return "data"


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("data")
        fb.field("cane_id")
        fb.field("farmaco_id")
        fb.field("note")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
