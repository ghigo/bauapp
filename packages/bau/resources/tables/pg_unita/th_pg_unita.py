#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method
from datetime import datetime, timedelta


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("programma_id")
        r.fieldcell("cane_id")
        r.fieldcell("tipo_attivita_id")
        r.fieldcell("descrizione")
        r.fieldcell("ora_inizio")
        r.fieldcell("ora_fine")

    def th_order(self):
        return "programma_id"

    def th_query(self):
        return dict(column="programma_id", op="contains", val="")


class ViewFromProgramma(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("@staff_coinvolti.@staff_id.nome", name="Volontaro", width="15em")
        r.fieldcell("ora_inizio", name="Quando", width="7em")
        r.fieldcell("template_cell", name="Chi e con chi", width="100%")

    def th_order(self):
        return "ora_inizio"

    def th_options(self):
        return dict(grid_canSort=False)

    def th_query(self):
        return dict(column="programma_id", op="contains", val="")


class Form(BaseComponent):
    def th_form(self, form):
        form.record


class FormFromProgramma(BaseComponent):
    def th_form(self, form):
        bc = form.center.borderContainer()
        top = bc.contentPane(region="top", datapath=".record")
        fb = top.div(margin="5px", margin_right="20px").formbuilder(
            cols=2, border_spacing="4px", colswidth="auto", width="100%"
        )
        fb.field(
            "tipo_attivita_id",
            tag="hdbselect",
            selected_descrizione=".$descrizione_attivita",
        )
        fb.field(
            "cane_id",
            validate_notnull="^.@tipo_attivita_id.richiede_cane",
            validate_remote=self.db.table("bau.pg_unita").controlloCane,
            validate_remote_programma_id="=.programma_id",
            validate_remote_area="=.area",
            validate_remote_ora_inizio="=.ora_inizio",
            validate_remote_ora_fine="=.ora_fine",
        )
        fb.field("ora_inizio")
        fb.field("ora_fine")
        fb.field(
            "area_id",
            validate_remote=self.db.table("bau.pg_unita").controlloArea,
            validate_remote_programma_id="=.programma_id",
            validate_remote_cane_id="=.cane_id",
            validate_remote_ora_inizio="=.ora_inizio",
            validate_remote_ora_fine="=.ora_fine",
        )
        fb.dataFormula(
            ".colore",
            "getRandomColor()",
            _if="isnewrecord",
            isnewrecord="=#FORM.controller.is_newrecord",
            _fired="^#FORM.controller.loaded",
        )
        fb.field("colore", tag="colorTextBox")
        fb.dataFormula(
            ".descrizione",
            "descrizione_attivita",
            _if="descrizione_attivita && !descrizione",
            descrizione="=.descrizione",
            descrizione_attivita="^.$descrizione_attivita",
        )
        fb.field(
            "descrizione", tag="simpleTextArea", height="80px", colspan=2, editor=True
        )
        th = bc.contentPane(region="center").inlineTableHandler(
            relation="@staff_coinvolti",
            viewResource="ViewFromUnita",
            grid_selfDragRows=True,
        )
        bc.dataController(
            "grid.gridEditor.addNewRows([{staff_id:staff_iniziale_id}])",
            staff_iniziale_id="=#FORM.record.staff_iniziale_id",
            isnewrecord="=#FORM.controller.is_newrecord",
            grid=th.view.grid.js_widget,
            _fired="^#FORM.controller.loaded",
            _delay=1,
        )  # non facendo nessuna rpc dovrebbe essere ok

    def th_bottom_custom(self, bottom):
        bar = bottom.bar.replaceSlots("revertbtn", "revertbtn,10,deletebtn")
        bar.deletebtn.slotButton(
            u"Elimina attività", action="this.form.deleteItem({destPkey:'*dismiss*'})"
        )

    @public_method
    def th_onLoading(self, record, newrecord, loadingParameters, recInfo, **kwargs):
        pg_staff = self.db.table("bau.pg_staff")
        programma_id = record["programma_id"]

        unita_staff_tbl = self.db.table("bau.pg_unita_staff")
        pg_staff_dict = pg_staff.query(
            columns="$staff_id,$ora_inizio_calc",
            where="$programma_id=:pd",
            pd=programma_id,
        ).fetchAsDict("staff_id")

        if newrecord:
            staff_id = loadingParameters.get("staff_id")

            ora_inizio = unita_staff_tbl.readColumns(
                columns="$ora_fine",
                where="$staff_id=:st AND @unita_id.programma_id=:pd",
                st=staff_id,
                pd=programma_id,
                order_by="$ora_fine desc",
                limit=1,
            )
            if not ora_inizio:
                ora_inizio = pg_staff_dict.get(staff_id)["ora_inizio_calc"]
            record["ora_inizio"] = ora_inizio
            data = record["@programma_id.data"]
            ora_fine_default = datetime(
                data.year, data.month, data.day, ora_inizio.hour, ora_inizio.minute
            )
            ora_fine_default += timedelta(minutes=30)
            record["ora_fine"] = ora_fine_default.time()
            record["staff_iniziale_id"] = staff_id
        wherelist = ["@unita_id.programma_id=:pd", "$ora_inizio>=:oi"]
        if record["id"]:
            wherelist.append("$unita_id!=:uid")
        staff_indisponibili = unita_staff_tbl.query(
            columns="$staff_id",
            distinct=True,
            where=" AND ".join(wherelist),
            uid=record["id"],
            oi=record["ora_inizio"],
            pd=programma_id,
        ).fetch()
        # staff_disponibili = set(pg_staff_dict.keys())
        staff_indisponibili = [r["staff_id"] for r in staff_indisponibili]
        # staff_disponibili = staff_disponibili.difference(staff_indisponibili)
        record["$staff_disponibili"] = list(pg_staff_dict.keys())
        record["$staff_indisponibili"] = staff_indisponibili

    def th_options(self):
        return dict(dialog_height="500px", dialog_width="700px")
