#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data_inizio")
        r.fieldcell("data_fine")
        r.fieldcell("cane_id")
        r.fieldcell("farmaco_id")
        r.fieldcell("dosaggio")
        r.fieldcell("orario")
        r.fieldcell("frequenza")

    def th_order(self):
        return "data_inizio"

    def th_query(self):
        return dict(column="data_inizio", op="contains", val="")


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("data_inizio")
        fb.field("data_fine")
        fb.field("cane_id")
        fb.field("farmaco_id")
        fb.field("dosaggio")
        fb.field("orario")
        fb.field("frequenza")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
