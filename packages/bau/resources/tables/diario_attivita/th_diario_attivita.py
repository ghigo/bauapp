#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("diario_id")
        r.fieldcell("tipo_attivita_id")
        r.fieldcell("campi_attivita")
        r.fieldcell("descrizione_attivita")

    def th_order(self):
        return "diario_id"

    def th_query(self):
        return dict(column="diario_id", op="contains", val="")


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("diario_id")
        fb.field("tipo_attivita_id")
        fb.field("campi_attivita")
        fb.field("descrizione_attivita")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
