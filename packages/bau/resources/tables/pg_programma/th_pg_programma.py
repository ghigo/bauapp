#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method
from gnr.core.gnrbag import Bag


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data", width="8em")
        r.fieldcell("@responsabile_id.nome", width="15em")
        r.fieldcell("ora_inizio", width="8em")
        r.fieldcell("ora_fine", width="8em")
        r.fieldcell("note", width="100%")

    def th_order(self):
        return "data"

    def th_queryBySample(self):
        return dict(
            fields=[
                dict(field="data", lbl="Data", width="10em"),
                dict(field="note", lbl="Note", width="20em"),
                dict(field="@responsabile_id.nome", lbl="Responsabile", width="20em"),
                dict(
                    field="@staff_coinvolti.@staff_id.nome",
                    lbl="Volontari",
                    width="20em",
                ),
            ],
            cols=4,
            isDefault=True,
        )


class Form(BaseComponent):
    py_requires = "component_programmazione:ProgrammazioneManager"

    def th_form(self, form):
        bc = form.center.borderContainer()
        fb = bc.contentPane(region="top", datapath=".record").formbuilder(
            cols=4, border_spacing="4px"
        )
        fb.field("data")
        fb.field("responsabile_id")
        fb.field("ora_inizio")
        fb.field("ora_fine")
        fb.field("note", colspan=4, width="100%", tag="simpleTextArea")

        tc = bc.tabContainer(region="center", margin="2px")
        self.configurazioniStaff(tc.borderContainer(title="Presenti"))
        self.programmazionePane(tc.borderContainer(title="Programmazione"))
        tc.contentPane(title="Pappe e terapie").inlineTableHandler(
            relation="@pappe",
            picker="cane_id",
            picker_viewResource="ViewPickerPappe",
            viewResource="ViewProgrammaPappe",
            grid_selfDragRows=True,
            addrow=False,
            pbl_classes=True,
            margin="2px",
            picker_width="700px",
            picker_height="400px",
            picker_defaults="dieta_sera:razione,terapia_sera:terapia,struttura_id",
        )
        tc.contentPane(title="Note pappe e terapie").ckeditor(
            value="^#FORM.record.indicazioni_pappe", toolbar="simple"
        )

    def configurazioniStaff(self, bc):
        pane = bc.contentPane(region="center")
        pane.inlineTableHandler(
            relation="@staff_coinvolti",
            picker="staff_id",
            addrow=False,
            grid_selfDragRows=True,
            picker_viewResource="ViewPicker",
            viewResource="ViewFromProgramma",
            pbl_classes=True,
            margin="2px",
        )

    def programmazionePane(self, bc):
        bc.contentPane(region="center").programmazioneManager()

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px", printMenu=True)
