#!/usr/bin/env python
# encoding: utf-8

from gnr.web.gnrbaseclasses import TableScriptToHtml

CURRENCY_FORMAT = "#,###.00"


class Main(TableScriptToHtml):
    maintable = "bau.pg_programma"

    def main(self):
        page = self.getNewPage()

        # principale = page.layout('principale',top=1,left=1,right=1,bottom=1,border_width=0)
        self.testata(
            page.div(
                height="15mm", style="font-size:16pt; font-weight:bold;color:#444;"
            )
        )
        self.righe(page)
        self.body.style(
            """.sezione_label{
                font-size:13pt; 
                font-weight:bold;
                text-align:left;
                color:#444;
                padding-left:5mm;
                margin-top:5mm;
            }
            .sotto_sezione{
                font-size:12pt; 
                color:#444;
                margin-top:1mm;
            }

            """
        )

        page.div(
            "Altre indicazioni",
            style="font-size:16pt; font-weight:bold;color:#444; text-align:center;margin-top:10mm;",
        )
        if self.record["indicazioni_pappe"]:
            page.div("%s::HTML" % self.record["indicazioni_pappe"])
        # self.piede(principale.row(height=10))

    def testata(self, box):
        tr = box.div(
            "Indicazioni pappe in data %s. Responsabile %s"
            % (self.field("data"), self.field("@responsabile_id.nome")),
            style="text-align:center; color:#444; font-weight:bold; font-size:15pt;",
        )

    def righe(self, page):
        for pappa in self.record["@pappe"].values():
            box = page.div(
                style="border: .3mm solid silver;margin-top:5mm; border-radius:2mm"
            )
            tr = (
                box.div(style="border-bottom: .3mm solid silver;")
                .table(width="100%")
                .tbody()
                .tr()
            )
            tr.td(
                "%i) %s" % (pappa["_row_count"], pappa["@cane_id.nome"]),
                width="50%",
                style="font-weight:bold;",
            )
            tr.td(
                pappa["@struttura_id.hierarchical_nome"],
                width="50%",
                style="text-align:right;",
            )
            content = box.div(padding="2mm")
            content.div("%s ::HTML" % pappa["razione"], _class="sotto_sezione")
            if pappa["terapia"]:
                content.div(
                    "%s::HTML" % pappa["terapia"],
                    _class="sotto_sezione",
                    style="background:RGBA(255, 253, 123, 0.40);padding:2mm;border:.3mm solid red;",
                )

    def outputDocName(self, ext=""):
        n = self.getData("record.id")
        return "%s.%s" % (n.replace(".", "_").replace("/", "_"), ext)
