#!/usr/bin/env pythonw
# -*- coding: UTF-8 -*-
#
#


from gnr.web.gnrbaseclasses import TableScriptToHtml
from gnr.core.gnrbag import Bag, BagCbResolver
import re


class Main(TableScriptToHtml):
    maintable = "bau.programma"
    rows_path = "rows"
    row_mode = "attribute"
    page_debug = False
    page_width = 220
    page_height = 297
    page_header_height = 0  # I don't need pageHeader is the callbac
    page_footer_height = 0  # I don't need
    doc_header_height = 30  # headerHeight docHeader is the callback
    doc_footer_height = 0  # footerHeight docFooter is the callback
    grid_header_height = 6.2  # bodyHeaderHeight
    grid_footer_height = 0  #
    grid_row_height = 32

    grid_columns = [
        dict(
            mm_width=50,
            name="Volontari",
            header_style="font-size:13pt;font-weight:bold; background:yellow;text-align:center;",
        ),
        dict(
            mm_width=15,
            name="Quando",
            header_style="font-size:13pt;font-weight:bold; background:yellow;text-align:center;",
        ),
        dict(
            mm_width=25,
            name="Dove",
            header_style="font-size:13pt;font-weight:bold; background:yellow;text-align:center;",
        ),
        dict(
            mm_width=0,
            name="Con chi e cosa",
            header_style="font-size:13pt;font-weight:bold; background:yellow;text-align:center;",
        ),
    ]

    def defineCustomStyles(self):
        self.body.style(""".margin_cell{margin:1mm;}""")

    def calcRowHeight(self):
        rowdata = self.currRowDataNode.attr
        volontari = rowdata["volontari_full"]
        descrizione = rowdata["descrizione"] or "<div>&nbsp</div>"
        calcheight = 12 * max(
            (
                (len(descrizione) / 70),
                (len(volontari) / 25),
                (len(rowdata["area_nome"] or "-") / 12),
                1,
            )
        )
        return calcheight

    def onNewRow(self):
        _prev_ora_inizio = getattr(self, "_prev_ora_inizio", None)
        ora_inizio = self.rowField("ora_inizio")
        result = 0
        if _prev_ora_inizio and ora_inizio > _prev_ora_inizio:
            row = self.copyValue("body_grid").row(height=3)
            row.cell(style="background:#444")
            result = 5
        self._prev_ora_inizio = ora_inizio
        return result

    def docHeader(self, header):
        layout = header.layout(
            name="header",
            um="mm",
            lbl_class="smallCaption",
            top=1,
            bottom=1,
            left=1,
            right=1,
            lbl_height=3,
            border_width=0.3,
            border_color="gray",
            style="line-height:6mm;text-align:left;text-indent:2mm;",
        )
        row = layout.row(height=10)
        row.cell("Organizzazione canile")
        row.cell(self.field("data"), lbl="Data", width=18)
        row.cell(self.field("@responsabile_id.nome"), lbl="Responsabile")
        row.cell(self.field("ora_inizio"), lbl="Apertura", width=18)
        row.cell(self.field("ora_fine"), lbl="Chiusura", width=18)
        row = layout.row()
        row.cell(self.field("note"), lbl="Annotazioni")

        # row.cell(self.pageCounter(), lbl='Pagina', width=12,content_class='aligned_right')

    def gridLayout(self, body):
        # here you receive the body (the center of the page) and you can define the layout
        # that contains the grid
        return body.layout(
            name="rowsL",
            um="mm",
            border_color="gray",
            top=1,
            bottom=1,
            left=1,
            right=1,
            border_width=0.3,
            style="line-height:5mm;text-align:left;font-size:12pt",
        )

    def prepareRow(self, row):
        # this callback prepare the row of the maingrid
        ora_inizio = self.rowField("ora_inizio")
        style_cell = "border-bottom-style:dotted;"
        self.rowCell(
            "volontari_full",
            style=style_cell,
            content_class="margin_cell",
            white_space="wrap",
        )
        self.rowCell(
            "ora_inizio", style=style_cell, content_class="margin_cell", format="HH:mm"
        )
        self.rowCell(
            "area_nome",
            style=style_cell,
            content_class="margin_cell",
            white_space="wrap",
        )
        cane = self.rowField("cane_nome")
        descrizione = self.rowField("descrizione") or ""
        descrizione = descrizione.replace("\n", "")
        if cane:
            if descrizione.startswith("<div>"):
                descrizione = descrizione.replace("<div>", "<div><b>%s: </b>" % cane, 1)
            else:
                descrizione = "<b>%s:</b>%s" % (cane, descrizione)
        desc = "%s ::HTML" % descrizione
        self.rowCell(
            value=desc,
            style=style_cell,
            content_class="margin_cell",
            white_space="wrap",
        )

    def mainLayout(self, page):
        style = """font-family:"Arial Narrow";
            text-align:left;
            line-height:4mm;
            font-size:13pt;"""
        return page.layout(
            name="pageLayout",
            height=self.page_height,
            um="mm",
            top=0,
            right=0,
            bottom=0,
            left=0,
            border_width=0,
            lbl_height=4,
            lbl_class="caption",
            style=style,
        )

    def onRecordLoaded(self):
        """override this"""
        selection = (
            self.db.table("bau.pg_unita")
            .query(
                columns="""*,$volontari_full,
                                                                $descrizione,$area_nome,
                                                                $cane_nome""",
                where="$programma_id=:pid AND $n_staff>0",
                order_by="$ora_inizio",
                pid=self.record["id"],
            )
            .selection()
        )
        if not selection:
            return False
        self.setData("rows", selection.output("grid"))
