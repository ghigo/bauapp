# -*- coding: UTF-8 -*-

# test_special_action.py
# Created by Francesco Porcari on 2010-07-02.
# Copyright (c) 2010 Softwell. All rights reserved.

from gnr.web.batch.btcprint import BaseResourcePrint

caption = "Stampa pappe"
tags = "user"
description = "Stampa pappe"


class Main(BaseResourcePrint):
    batch_prefix = "pappe"
    batch_title = "Stampa pappe"
    batch_cancellable = True
    batch_immediate = "download"
    batch_delay = 0.5
    html_res = "html_res/pappe"
