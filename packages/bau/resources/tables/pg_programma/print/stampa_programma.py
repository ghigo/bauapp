# -*- coding: UTF-8 -*-

# test_special_action.py
# Created by Francesco Porcari on 2010-07-02.
# Copyright (c) 2010 Softwell. All rights reserved.

from gnr.web.batch.btcprint import BaseResourcePrint

caption = "Stampa Programma"
tags = "user"
description = "Stampa Programma"


class Main(BaseResourcePrint):
    batch_prefix = "pg"
    batch_title = "Stampa Programma"
    batch_cancellable = True
    batch_immediate = "download"
    batch_delay = 0.5
    html_res = "html_res/prospetto"
