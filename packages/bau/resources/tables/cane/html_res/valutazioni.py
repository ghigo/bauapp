#!/usr/bin/env python
# encoding: utf-8

from gnr.web.gnrbaseclasses import TableScriptToHtml

CURRENCY_FORMAT = "#,###.00"


class Main(TableScriptToHtml):
    maintable = "bau.cane"

    def main(self):
        page = self.getNewPage()

        # principale = page.layout('principale',top=1,left=1,right=1,bottom=1,border_width=0)
        self.testata(
            page.div(
                height="15mm", style="font-size:16pt; font-weight:bold;color:#444;"
            )
        )
        self.righe(page)
        self.body.style(
            """.sezione_label{
                font-size:13pt; 
                font-weight:bold;
                text-align:left;
                color:#444;
                padding-left:5mm;
                margin-top:2mm;
            }
            .sotto_sezione{
                font-size:12pt; 
                color:#444;
                margin-top:1mm;
            }

            """
        )
        if self.record["resoconto"]:
            page = self.getNewPage()
            page.div(
                "Resoconto libero",
                style="font-size:16pt; font-weight:bold;color:#444; text-align:center;",
            )
            page.div("%s::HTML" % self.record["resoconto"])
        # self.piede(principale.row(height=10))

    def testata(self, box):
        tr = (
            box.table(width="100%", border_width=0, border_collapse="collapse")
            .tbody()
            .tr()
        )
        tr.td("Valutazioni correnti - %s" % self.field("nome"))

    def valutazioni_condition(self):
        if self.db.currentEnv.get("data_limite"):
            return "($cane_id=:cid AND $passata_in_data IS NOT TRUE) AND $data<=:env_data_limite AND ($data_chiusura IS NULL OR $data_chiusura <=:env_data_limite)"
        return "($cane_id=:cid AND $passata IS NOT TRUE AND $data_chiusura IS NULL)"

    def righe(self, page):
        valutazioni = (
            self.db.table("bau.cane_valutazione")
            .query(
                where=self.valutazioni_condition(),
                cid=self.record["id"],
                order_by="@valutazione_tipo_id.titolo",
            )
            .selection()
            .output("records")
        )

        for valutazione in valutazioni:
            calcolo = False
            if valutazione["risultato_valutazione"]:
                calcolo = True
                ri = int(valutazione["risultato_valutazione"])
                ris_testo = ""
                if ri < 3:
                    ris_testo = "FACILE DA GESTIRE"
                elif ri < 5:
                    ris_testo = "POTENZIALMENTE FACILE DA GESTIRE"
                elif ri < 6:
                    ris_testo = "POTENZIALMENTE FACILE, MA RICHIEDE TEMPO E LAVORO PER INSERIMENTO IN FAMIGLIA"
                elif ri < 7:
                    ris_testo = "PROPRIETARIO ESPERTO"
                elif ri < 8:
                    ris_testo = "PROBLEMATICO"
                else:
                    ris_testo = "NON ADOTTABILE ATTUALMENTE"
                ris_testo = "%s (%s)" % (
                    ris_testo,
                    self.toText(
                        valutazione["risultato_valutazione"], format="#,###.00"
                    ),
                )
                page.div(
                    "[%s] - %s: %s"
                    % (
                        self.toText(valutazione["data"]),
                        valutazione["@valutazione_tipo_id.titolo"],
                        ris_testo,
                    ),
                    _class="sezione_label",
                )
            else:
                page.div(
                    "[%s] - %s"
                    % (
                        self.toText(valutazione["data"]),
                        valutazione["@valutazione_tipo_id.titolo"],
                    ),
                    _class="sezione_label",
                )
                subsection = page.div(
                    style="border-top: 1mm solid RGBA(234, 29, 36, 1.00); padding:3mm; margin:1mm;"
                )
                subfields = self.db.table("bau.cane_valutazione_tipo").df_subFieldsBag(
                    valutazione["valutazione_tipo_id"]
                )
                data = valutazione["dati_valutazione"]
                for pagename, content in subfields.items():
                    if not content:
                        if data[pagename]:
                            subsection.div(
                                "%s ::HTML" % data[pagename],
                                _class="sotto_sezione",
                                style="padding-left:10mm;padding-right:10mm;font-size:11pt;",
                            )
                        continue
                    codici_attivi = [r for r in content.keys() if data[r] is not None]
                    if not codici_attivi:
                        continue
                    for k in codici_attivi:
                        n = data.getNode(k)
                        if n and n.value:
                            text = n.value
                            text = text.replace("\n", "<br/>")
                            box = subsection.div(
                                style="border:1px solid silver; margin:1mm;border-radius:.5mm;padding:2mm;"
                            )
                            box.div(
                                "<b>%s:</b> ::HTML" % n.getAttr("_valuelabel"),
                                _class="sotto_sezione",
                            )
                            box.div(
                                "%s ::HTML" % text,
                                _class="sotto_sezione",
                                style="padding-left:10mm;padding-right:10mm;font-size:11pt;",
                            )

    def outputDocName(self, ext=""):
        n = self.getData("record.id")
        return "%s.%s" % (n.replace(".", "_").replace("/", "_"), ext)
