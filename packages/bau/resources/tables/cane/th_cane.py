#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("nome")
        r.fieldcell("eta")
        r.fieldcell("sesso")
        r.fieldcell("data_sterilizzazione")
        r.fieldcell("bollino_codice")
        r.fieldcell("razza_id")
        r.fieldcell("taglia")
        r.fieldcell("data_arrivo")
        r.fieldcell("struttura_id", name="Box")
        r.fieldcell("associazione_codice")

    def th_order(self):
        return "nome"

    # def th_options(self):
    #    return dict(virtualStore=False,extendedQuery=False)

    def th_queryBySample(self):
        return dict(
            fields=[
                dict(field="nome", lbl="Cane", width="10em"),
                dict(field="@razza_id.hierarchical_nome", lbl="Razza", width="15em"),
                dict(
                    field="bollino_codice",
                    lbl="Bollino",
                    width="10em",
                    tag="dbSelect",
                    dbtable="bau.bollino",
                    hasDownArrow=True,
                ),
                dict(field="@staff_responsabili.@staff_id.nome", lbl="Volontari"),
            ],
            cols=4,
            isDefault=True,
        )


class ViewPickerPappe(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("nome", width="8em")
        r.fieldcell("struttura_id", name="Box", width="12em")
        r.fieldcell("associazione_codice", width="10em")
        r.fieldcell("dieta_sera", width="20em")
        r.fieldcell("terapia_sera", width="20em")

    def th_order(self):
        return "nome"


class Form(BaseComponent):
    py_requires = """gnrcomponents/attachmanager/attachmanager:AttachManager,
                      gnrcomponents/dynamicform/dynamicform:DynamicForm"""

    def th_form(self, form):
        bc = form.center.borderContainer()
        top = bc.contentPane(region="top", datapath=".record")
        fb = top.div(margin="10px", margin_right="30px").formbuilder(
            cols=4,
            border_spacing="4px",
            width="100%",
            colswidth="auto",
            fld_width="100%",
        )
        fb.field("nome", colspan=2, validate_notnull=True)
        fb.field("associazione_codice", hasDownArrow=True)
        fb.img(
            src="^.foto",
            crop_height="140px",
            crop_width="140px",
            crop_margin_left="20px",
            crop_border="2px dotted silver",
            crop_rounded=6,
            edit=True,
            placeholder=True,
            upload_folder="vol:avatar/cane",
            upload_filename="=#FORM.record.id",
            colspan=1,
            rowspan=7,
        )

        fb.field("data_nascita", placeholder="anno o data", width="7em")
        fb.field("data_nascita_esatta", label="Esatta", lbl="")

        fb.field("sesso", width="7em")
        fb.field("data_sterilizzazione", width="7em")
        fb.field("data_arrivo", width="7em")
        fb.field("microchip")
        fb.field("razza_id", tag="hdbselect", selected_taglia=".taglia")
        fb.field("taglia", width="7em")
        fb.field("peso", width="7em")
        fb.field("altezza", width="7em")
        fb.field("bollino_codice")
        fb.field("struttura_id", tag="hdbselect")
        fb.div("&nbsp;", colspan=3)
        tc = bc.tabContainer(margin="2px", region="center")
        self.cane_staff(tc.contentPane(title="Volontari"))
        self.cane_come_agire(tc)
        self.cane_vaccini_antipar(tc)
        self.cane_scheda_clinica(tc)

        self.cane_dieta(tc.contentPane(title="Dieta e terapie", datapath=".record"))

        self.cane_valutazioni(tc)
        tc.contentPane(title="!![it]Allegati").attachmentGallery()
        tc.contentPane(title="Diari volontari").dialogTableHandler(
            relation="@diari", formResource="FormFromCane", viewResource="ViewFromCane"
        )

    def cane_come_agire(self, tc):
        tc.contentPane(title="Come agire", overflow="hidden").ckeditor(
            value="^.record.come_agire",
            toolbar="simple",
            disabled='^gnr.rootenv.user_group_code?=#v=="vol"',
        )

    def cane_dieta(self, pane):
        fb = pane.div(margin="10px", margin_right="30px").formbuilder(
            cols=1,
            border_spacing="4px",
            width="100%",
            colswidth="auto",
            fld_width="100%",
        )
        fb.field("dieta_mattino", height="100px", tag="simpleTextArea", editor=True)
        fb.field("dieta_sera", height="100px", tag="simpleTextArea", editor=True)
        fb.field("terapia_mattino", height="100px", tag="simpleTextArea", editor=True)
        fb.field("terapia_sera", height="100px", tag="simpleTextArea", editor=True)

    def cane_staff(self, pane):
        pane.inlineTableHandler(
            relation="@staff_responsabili",
            picker="staff_id",
            addrow=False,
            grid_selfDragRows=True,
            picker_viewResource="ViewPicker",
            viewResource="ViewFromCane",
            pbl_classes=True,
            margin="2px",
        )

    def cane_valutazioni(self, tc):
        tc = tc.tabContainer(title="Valutazioni", margin="2px")

        tc.contentPane(title="Modifica").dialogTableHandler(
            relation="@valutazioni",
            viewResource="ViewFromCane",
            grid_selfsubscribe_onNewDatastore="FIRE #FORM.reload_valutazioni;",
            export=True,
            formResource="FormFromCane",
            pbl_classes=True,
            margin="2px",
        )

        frame = tc.framePane(title="Riepilogo")
        frame.dataController(
            "FIRE #FORM.reload_valutazioni", _fired="^#FORM.controller.loaded"
        )
        frame.documentFrame(
            resource="bau.cane:html_res/valutazioni",
            pkey="^#FORM.record.id",
            dbenv_data_limite="^#FORM.data_limite",
            html=True,
            _reloader="^#FORM.reload_valutazioni",
            iframe_nodeId="riepilogo_iframe",
            _if="pkey",
            _delay=10,
        )
        topbar = frame.top.slotToolbar("*,fbdatalimite,stampa_riepilogo,5")
        topbar.fbdatalimite.formbuilder(border_spacing="0px").dateTextBox(
            value="^#FORM.data_limite",
            lbl="Data riferimento",
            width="7em",
            parentForm=False,
            validate_onAccept="FIRE #FORM.reload_valutazioni;",
        )
        topbar.stampa_riepilogo.slotButton(
            "Stampa riepilogo",
            action="var iframe = genro.domById('riepilogo_iframe').contentWindow.print()",
        )

    def cane_vaccini_antipar(self, tc):
        bc = tc.borderContainer(title="Vaccinazioni e Antiparassitari")
        bc.contentPane(region="left", width="50%").inlineTableHandler(
            relation="@cane_vaccinazioni",
            title="Vaccinazioni",
            picker_uniqueRow=False,
            viewResource="ViewFromCane",
            picker="vaccino_id",
            addrow=False,
            pbl_classes=True,
            margin="2px",
        )

        bc.contentPane(region="center").inlineTableHandler(
            relation="@cane_trattamenti_antiparassitari",
            title="Trattamenti antiparassitari",
            picker_uniqueRow=False,
            viewResource="ViewFromCane",
            picker="farmaco_id",
            picker_condition="@farmaco_tipo_id.@root_id.categoria=:ct",
            picker_condition_ct="AP",
            picker_structure_field="farmaco_tipo_id",
            picker_structure_condition="@root_id.categoria=:ct",
            picker_structure_condition_ct="AP",
            picker_structure_condition_fired="^gnr.onStart",
            addrow=False,
            pbl_classes=True,
            margin="2px",
        )

    def cane_scheda_clinica(self, tc):
        tc.contentPane(
            title="Scheda clinica", datapath="#FORM.record"
        ).dynamicFieldsPane("sierologia")

    def th_options(self):
        return dict(
            dialog_height="400px",
            dialog_width="600px",
            defaultPrompt=dict(
                title="Nuovo cane",
                fields=[
                    dict(
                        value="^.nome",
                        tag="textbox",
                        lbl="Nome",
                        width="15em",
                        validate_notnull=True,
                        validate_case="C",
                    ),
                    dict(
                        value="^.associazione_codice",
                        tag="dbselect",
                        lbl="Associazione",
                        width="15em",
                        dbtable="bau.associazione",
                        hasDownArrow=True,
                    ),
                    dict(
                        value="^.data_nascita",
                        tag="dateTextBox",
                        lbl="Nascita",
                        width="7em",
                        placeholder="anno o data",
                    ),
                ],
                doSave=True,
            ),
        )


class FormFromVolontario(Form):
    def th_options(self):
        return dict(addrow=False, delrow=False, modal=True, printMenu=True)
