#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method
from gnr.core.gnrbag import Bag


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data", width="7em")
        r.fieldcell("titolo", width="20em")
        r.fieldcell("tipo_evento", width="10em")
        r.fieldcell("luogo", width="10em")
        r.fieldcell("indirizzo", width="20em")
        r.fieldcell("localita", width="10em")
        r.fieldcell("descrizione", width="20em")
        r.fieldcell("ora_inizio", width="7em")
        r.fieldcell("ora_fine", width="7em")
        r.fieldcell("mostra", width="7em")

    def th_order(self):
        return "data"

    def th_query(self):
        return dict(column="data", op="equal", val="oggi;")


class ViewFromVolontario(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data", width="8em")
        r.fieldcell("titolo", width="20em")
        r.fieldcell("descrizione", width="20em")
        r.fieldcell("luogo", width="15em")
        r.fieldcell("indirizzo", width="15em")
        r.fieldcell("localita", width="10em")
        r.fieldcell("ora_inizio", width="7em")
        r.fieldcell("ora_fine", width="7em")
        r.fieldcell(
            "disponibilita_id",
            width="7em",
            _customGetter="""function(row){
            return row.disponibilita_id?'<span style="color:green;">Presente</span>':'<span style="color:red;"></span>';
        }""",
            name=" ",
        )
        r.cell(
            "disp",
            calculated=True,
            format_isbutton=u"Modifica disponibilità",
            format_buttonclass="buttonInGrid",
            format_onclick="""var row = this.widget.rowByIndex($1.rowIndex);
                                     genro.publish("edit_disponibilita",{evento_id:row._pkey,disponibilita_id:row.disponibilita_id});
                                     """,
            name=" ",
            width="10em",
        )

    def th_order(self):
        return "data"


class Form(BaseComponent):
    py_requires = "gnrcomponents/timesheet_viewer/timesheet_viewer:TimesheetViewer"

    def th_form(self, form):
        bc = form.center.borderContainer()
        top = bc.borderContainer(region="top", height="220px", datapath=".record")
        fb = top.roundedGroup(title="Dati evento", region="center").formbuilder(
            cols=2, border_spacing="4px"
        )
        fb.field("data", width="8em")
        fb.field("tipo_evento")
        fb.field("ora_inizio", width="8em")
        fb.field("ora_fine", width="8em")

        fb.field("titolo", colspan=2, width="100%")
        fb.field("luogo", colspan=2, width="100%")
        fb.field("mostra", html_label=True, colspan=2)
        fb.geoCoderField(
            value="^.indirizzo",
            width="100%",
            colspan=2,
            lbl="Indirizzo",
            selected_position=".geocoords",
            ghost=u"Strada, Numero, Località",
        )
        fb.field("descrizione", width="100%", colspan=2)

        top.roundedGroup(title="Mappa", region="right", width="330px").GoogleMap(
            map_center="^.indirizzo",
            centerMarker=True,
            height="200px",
            width="300px",
            margin="5px",
        )

        tc = bc.tabContainer(region="center", margin="2px")
        tc.contentPane(title="Partecipanti").inlineTableHandler(
            relation="@partecipanti",
            picker="staff_id",
            addrow=False,
            grid_selfDragRows=True,
            picker_viewResource="ViewPicker",
            viewResource="ViewFromEvento",
            pbl_classes=True,
            margin="2px",
        )

        th = tc.contentPane(title="Banchetti").dialogTableHandler(
            relation="@banchetti",
            pbl_classes=True,
            margin="2px",
            default_ora_inizio="=#FORM/parent/#FORM.record.ora_inizio",
            default_ora_fine="=#FORM/parent/#FORM.record.ora_fine",
        )
        th.view.top.bar.replaceSlots("#", "#,resourcePrints,5")
        self.programmazioneBanchetti(
            tc.borderContainer(title="Programmazione banchetti")
        )

    def programmazioneBanchetti(self, bc):
        bc.dataRpc(
            "#FORM.programmazione",
            self.getTurniStaff,
            evento_id="=#FORM.record.id",
            data_evento="=#FORM.record.data",
            _if="data_evento && evento_id",
            _fired="^.refresh",
        )
        form_turno = self.formTurno(bc).js_form
        bc.dataController(
            """frm.load({destPkey:turno_id})""",
            frm=form_turno,
            subscribe_modifica_turno=True,
        )
        bc.dataController(
            """
        frm.newrecord({banchetto_id:banchetto_id,staff_id:staff_id})
        """,
            frm=form_turno,
            subscribe_nuovo_turno=True,
        )

        frame = bc.contentPane(region="center").timesheetViewer(
            region="center",
            value="^#FORM.programmazione",
            selfsubscribe_edit_timesheet="""
                                PUBLISH nuovo_turno = {banchetto_id:$1.banchetto_id,staff_id:$1.staff_id};
                                """,
            minute_height=2,
            selected_date="=#FORM.record.data",
            selfsubscribe_edit_slot="PUBLISH modifica_turno = {turno_id:$1.turno_id}",
            slotFiller="timetable",
            work_start="=#FORM.record.ora_inizio?=#v?(#v.getHours()):13",
            work_end="=#FORM.record.ora_fine?=#v?(#v.getHours()+1):19",
            viewerMode="singleDay",
            slot_duration=30,
        )
        bc.onDbChanges(
            """FIRE .refresh""",
            table="bau.banchetto",
            evento_id="^#FORM.controller.loaded",
        )
        bc.onDbChanges("""FIRE .refresh""", table="bau.banchetto_turno")

    def formTurno(self, parent):
        return parent.thFormHandler(
            table="bau.banchetto_turno",
            formResource="FormFromEvento",
            modal=True,
            dialog_height="200px",
            dialog_width="420px",
            parentForm=False,
            formId="banchetto_turno",
            datapath="#FORM.banchetto_turno",
        )

    @public_method
    def getTurniStaff(self, evento_id=None, data_evento=None):
        result = Bag()
        banchetti = (
            self.db.table("bau.banchetto")
            .query(
                where="$evento_id=:pid",
                pid=evento_id,
                columns="""$id,$nome,$ora_inizio,$ora_fine""",
                order_by="$nome",
            )
            .fetchAsDict("nome", ordered=True)
        )

        staff_turno_grouped = (
            self.db.table("bau.banchetto_turno_staff")
            .query(
                where="$evento_id=:pid",
                columns="""$banchetto,$banchetto_id,$turno_id,
                                                    $ora_inizio,$ora_fine,
                                                    $volontari,
                                                    $colore_turno""",
                pid=evento_id,
                order_by="$ora_inizio",
            )
            .fetchGrouped("banchetto")
        )
        tpl = """<div class='cls_vol' style="margin-top:10px;">$volontari</div>"""
        channels = banchetti.items()
        calcontent = Bag()
        daylabel = self.toText(data_evento, format="yyyy_MM_dd")
        programma = result.setItem(daylabel, calcontent, day=data_evento)

        for banchetto, banchetto_rec in channels:
            turno_volontario = staff_turno_grouped.get(banchetto, [])
            val = Bag()
            turni_banchetto = banchetti[banchetto]
            calcontent.setItem(
                banchetto,
                val,
                time_start=turni_banchetto["ora_inizio"],
                time_end=turni_banchetto["ora_fine"],
                banchetto_id=banchetto_rec["id"],
            )
            for r in turno_volontario:
                val.setItem(
                    self.toText(r["ora_inizio"], format="HH_mm"),
                    None,
                    time_start=r["ora_inizio"],
                    time_end=r["ora_fine"],
                    turno_id=r["turno_id"],
                    volontari=r["volontari"],
                    background_color=r["colore_turno"],
                    template=tpl,
                )
        return result, dict(channels=[v["nome"] for k, v in banchetti.items()])

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
