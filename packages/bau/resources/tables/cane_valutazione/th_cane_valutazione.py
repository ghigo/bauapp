#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class ViewFromCane(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data", width="7em", name="Data")
        r.fieldcell("valutazione_tipo_id", width="10em", name="Valutazione")
        r.fieldcell("dati_valutazione", width="40em", name="Dati raccolti")
        r.fieldcell(
            "risultato_valutazione", width="6em", name="Risultato", color="darkred"
        )

    def th_order(self):
        return "data"

    def th_top_custom(self, top):
        top.bar.replaceSlots("vtitle", "sections@statoval")

    def th_sections_statoval(self):
        return [
            dict(code="correnti", caption="Correnti", condition="$passata IS NOT TRUE AND $data_chiusura IS NULL"),
            dict(code="passate", caption="Passate", condition="$passata IS TRUE OR $data_chiusura IS NOT NULL"),
        ]


class FormFromCane(BaseComponent):
    py_requires = "gnrcomponents/dynamicform/dynamicform:DynamicForm"

    def th_form(self, form):
        frame = form.center.framePane(datapath=".record")
        frame.center.contentPane(region="center", overflow="auto").dynamicFieldsPane(
            "dati_valutazione"
        )
        bar = frame.bottom.slotBar("*,chiudi,risultato,30", height="20x")
        bar.risultato.div(
            "^#FORM.record.risultato_valutazione", format="#,##.00", font_size="20px"
        )
        bar.chiudi.formbuilder().dateTextBox(value='^#FORM.record.data_chiusura',lbl='Chiusa')
        bar.dataRpc(
            "#FORM.record.risultato_valutazione",
            self.db.table("bau.cane_valutazione_tipo").calcolaRisultato,
            pkey="=#FORM.record.valutazione_tipo_id",
            dati_valutazione="^#FORM.record.dati_valutazione",
            _delay=100,
        )

    def th_options(self):
        return dict(
            dialog_parentRatio=0.95,
            defaultPrompt=dict(
                title="Nuova valutazione",
                fields=[
                    dict(
                        value="^.valutazione_tipo_id",
                        tag="dbselect",
                        lbl="Valutazione",
                        validate_notnull=True,
                        width="15em",
                        dbtable="bau.cane_valutazione_tipo",
                        hasDownArrow=True,
                    ),
                    dict(
                        value="^.data",
                        tag="dateTextBox",
                        lbl="Data",
                        width="7em",
                        validate_notnull=True,
                    ),
                ],
                doSave=True,
            ),
            modal=True,
        )
