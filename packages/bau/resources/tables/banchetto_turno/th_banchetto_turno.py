#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method
from datetime import time


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("banchetto_id")
        r.fieldcell("ora_inizio")
        r.fieldcell("ora_fine")
        r.fieldcell("note")

    def th_order(self):
        return "banchetto_id"

    def th_query(self):
        return dict(column="banchetto_id", op="contains", val="")


class Form(BaseComponent):
    def th_form(self, form):
        form.record


class FormFromEvento(BaseComponent):
    def th_form(self, form):
        fb = form.record.div(margin="10px").formbuilder(cols=3)
        fb.checkboxtext(
            value="^.staff_pkeys",
            colspan=3,
            cols=2,
            table="bau.staff",
            order_by="$nome",
            condition="@eventi.evento_id=:evt_id",
            condition_evt_id="^#FORM.record.@banchetto_id.evento_id",
        )
        fb.field("colore", tag="colorTextBox", width="6em")
        fb.field(
            "ora_inizio",
            width="6em",
            validate_onAccept="""if(value){
                    var d = new Date(value);
                    d.setHours(d.getHours()+1);
                    SET .ora_fine = d;
                }""",
        )
        fb.field("ora_fine", width="6em")
        fb.dataFormula(
            ".colore",
            "getRandomColor()",
            _if="isnewrecord",
            isnewrecord="=#FORM.controller.is_newrecord",
            _fired="^#FORM.controller.loaded",
        )
        fb.field("note", width="100%", colspan=3)

    def th_bottom_custom(self, bottom):
        bar = bottom.bar.replaceSlots("revertbtn", "revertbtn,10,deletebtn")
        bar.deletebtn.slotButton(
            u"Elimina turno", action="this.form.deleteItem({destPkey:'*dismiss*'})"
        )

    @public_method
    def th_onLoading(self, record, newrecord, loadingParameters, recInfo, **kwargs):
        if newrecord:
            banchetto_tbl = self.db.table("bau.banchetto")
            record["ora_inizio"] = banchetto_tbl.readColumns(
                columns="$ora_inizio_disponibile", pkey=record["banchetto_id"]
            )
            record["ora_fine"] = time(
                record["ora_inizio"].hour + 1, record["ora_inizio"].minute
            )
        else:
            record.setItem(
                "staff_pkeys",
                ",".join([v["staff_id"] for v in record["@staff_turno"].values()]),
                _sendback=True,
            )

    def th_options(self):
        return dict(dialog_height="200px", dialog_width="500px")
