#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("evento_id")
        r.fieldcell("nome")
        r.fieldcell("note")
        r.fieldcell("ora_inizio")
        r.fieldcell("ora_fine")

    def th_order(self):
        return "evento_id"

    def th_query(self):
        return dict(column="evento_id", op="contains", val="")


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record.div(margin="20px")
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        # fb.field('evento_id')
        fb.field("nome", colspan=2, validate_notnull=True, width="100%")
        fb.field("ora_inizio", width="7em")
        fb.field("ora_fine", width="7em")
        fb.field("note", colspan=2, width="100%", tag="simpleTextArea")

    def th_options(self):
        return dict(dialog_height="220px", dialog_width="300px", modal=True)
