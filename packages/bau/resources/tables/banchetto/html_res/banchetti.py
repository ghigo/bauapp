#!/usr/bin/env pythonw
# -*- coding: UTF-8 -*-
#
#


from gnr.web.gnrbaseclasses import TableScriptToHtml
from gnr.core.gnrbag import Bag, BagCbResolver
from dateutil import rrule
from datetime import datetime, timedelta
import re


class Main(TableScriptToHtml):
    maintable = "bau.banchetto"
    rows_path = "rows"
    row_mode = "attribute"
    page_debug = False
    page_width = 220
    page_height = 297
    page_header_height = 0  # I don't need pageHeader is the callbac
    page_footer_height = 0  # I don't need
    doc_header_height = 30  # headerHeight docHeader is the callback
    doc_footer_height = 0  # footerHeight docFooter is the callback
    grid_header_height = 6.2  # bodyHeaderHeight
    grid_footer_height = 0  #
    grid_row_height = 10

    grid_columns = [
        dict(
            mm_width=15,
            name="Dalle",
            header_style="font-size:13pt;font-weight:bold; background:green;text-align:center;",
        ),
        dict(
            mm_width=15,
            name="Alle",
            header_style="font-size:13pt;font-weight:bold; background:green;text-align:center;",
        ),
        dict(
            mm_width=0,
            name="Volontari",
            header_style="font-size:13pt;font-weight:bold; background:green;text-align:center;",
        ),
    ]

    def defineCustomStyles(self):
        self.body.style(""".margin_cell{margin:1mm;}""")

    def docHeader(self, header):
        layout = header.layout(
            name="header",
            um="mm",
            lbl_class="smallCaption",
            top=1,
            bottom=1,
            left=1,
            right=1,
            lbl_height=3,
            border_width=0.3,
            border_color="gray",
            style="line-height:6mm;text-align:left;text-indent:2mm;",
        )
        row = layout.row(height=10)
        row.cell(self.field("@evento_id.data"), lbl="Data", width=20)
        row.cell(self.field("nome"), lbl="Banchetto")
        row.cell(self.field("@evento_id.titolo"), lbl="Evento")
        row.cell(self.field("@evento_id.luogo"), lbl="Luogo")
        row.cell(self.field("ora_inizio"), lbl="Apertura", width=18)
        row.cell(self.field("ora_fine"), lbl="Chiusura", width=18)
        row = layout.row()
        row.cell(self.field("note"), lbl="Annotazioni")

        # row.cell(self.pageCounter(), lbl='Pagina', width=12,content_class='aligned_right')

    def gridLayout(self, body):
        # here you receive the body (the center of the page) and you can define the layout
        # that contains the grid
        return body.layout(
            name="rowsL",
            um="mm",
            border_color="gray",
            top=1,
            bottom=1,
            left=1,
            right=1,
            border_width=0.3,
            style="line-height:5mm;text-align:left;font-size:12pt",
        )

    def prepareRow(self, row):
        # this callback prepare the row of the maingrid
        style_cell = "border-bottom-style:dotted;"
        self.rowCell(
            "ora_inizio", style=style_cell, content_class="margin_cell", format="HH:mm"
        )
        self.rowCell(
            "ora_fine", style=style_cell, content_class="margin_cell", format="HH:mm"
        )
        self.rowCell(
            "volontari",
            style=style_cell,
            content_class="margin_cell",
            white_space="wrap",
        )

    def mainLayout(self, page):
        style = """font-family:"Arial Narrow";
            text-align:left;
            line-height:4mm;
            font-size:16pt;"""
        return page.layout(
            name="pageLayout",
            height=self.page_height,
            um="mm",
            top=0,
            right=0,
            bottom=0,
            left=0,
            border_width=0,
            lbl_height=4,
            lbl_class="caption",
            style=style,
        )

    def onRecordLoaded(self):
        """override this"""
        ora_inizio = self.record["ora_inizio"]
        ora_fine = self.record["ora_fine"]
        data = self.record["@evento_id.data"]

        dtstart = datetime(
            data.year, data.month, data.day, ora_inizio.hour, ora_inizio.minute
        )
        until = datetime(
            data.year, data.month, data.day, ora_fine.hour, ora_fine.minute
        )

        f = (
            self.db.table("bau.banchetto_turno")
            .query(
                columns="""*,$volontari,
                                                                $ora_inizio,$ora_fine""",
                order_by="$ora_inizio",
                where="$banchetto_id=:bid",
                bid=self.record["id"],
            )
            .fetch()
        )
        dict_ora_inizio = dict([(r["ora_inizio"], r) for r in f])
        dict_ora_fine = dict([(r["ora_fine"], r) for r in f])

        if not f:
            return False
        b = Bag()
        i = 0
        orari = [
            r.time()
            for r in rrule.rrule(
                rrule.MINUTELY, interval=30, dtstart=dtstart, until=until
            )
        ]
        turnocorrente = None
        for i, t in enumerate(orari[0:-1]):
            if not turnocorrente or not (
                turnocorrente["ora_inizio"] <= t
                and turnocorrente["ora_fine"] >= orari[i + 1]
            ):
                turnocorrente = dict_ora_inizio.get(t) or {}
            b.setItem(
                "r_%i" % i,
                None,
                volontari=turnocorrente.get("volontari", "Da assegnare"),
                ora_inizio=t,
                ora_fine=orari[i + 1],
            )
        self.setData("rows", b)
