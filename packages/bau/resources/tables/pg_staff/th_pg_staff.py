#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method


class ViewFromProgramma(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("_row_count", hidden=True, counter=True)

        r.fieldcell("staff_id", width="15em")
        r.fieldcell(
            "ora_inizio",
            _customGetter="""function(row){
            return row.ora_inizio || this.grid.sourceNode.getRelativeData('#FORM.record.ora_inizio');
        }""",
            edit=True,
            width="8em",
        )
        r.fieldcell(
            "ora_fine",
            _customGetter="""function(row){
            return row.ora_fine || this.grid.sourceNode.getRelativeData('#FORM.record.ora_fine');
        }""",
            edit=True,
            width="8em",
        )
        r.checkboxcolumn("automobile", name="Auto", width="5em")
        r.checkboxcolumn("diario", name="Diario", width="5em")
        r.fieldcell("note", edit=True, width="100%")

    def th_order(self):
        return "_row_count"


class Form(BaseComponent):
    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing="4px")
        fb.field("staff_id")
        fb.field("programma_id")

    def th_options(self):
        return dict(dialog_height="400px", dialog_width="600px")
