#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method
from datetime import datetime, timedelta


class View(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data")
        r.fieldcell("staff_id")
        r.fieldcell("cane_id", width="15em")
        r.fieldcell("tipo_attivita_id", width="20em")
        r.fieldcell("ora_inizio")
        r.fieldcell("ora_fine")

    def th_order(self):
        return "data"

    def th_queryBySample(self):
        return dict(
            fields=[
                dict(field="data", lbl="Data", width="10em"),
                dict(field="@cane_id.nome", lbl="Cane", width="15em"),
                dict(field="@staff_id.nome", lbl="Volontare", width="15em"),
            ],
            cols=3,
            isDefault=True,
        )


class ViewFromVolontario(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data", width="7em")
        r.fieldcell("cane_id", width="10em")
        r.fieldcell("tipo_attivita_id", width="10em")

    def th_order(self):
        return "data"


class ViewFromCane(BaseComponent):
    def th_struct(self, struct):
        r = struct.view().rows()
        r.fieldcell("data")
        r.fieldcell("staff_id", width="15em")
        r.fieldcell("tipo_attivita_id", width="20em")
        r.fieldcell("ora_inizio")
        r.fieldcell("ora_fine")

    def th_order(self):
        return "data"


class FormBase(BaseComponent):
    def stampaDiario(self, pane):
        pane.dataController(
            """var fileurl_nocache = fileurl?fileurl+'&nocache='+genro.time36Id():null;
                            SET #FORM.record.$fileurl_nocache = '';
                            var that = this;
                            setTimeout(function(){
                                that.setRelativeData('#FORM.record.$fileurl_nocache', fileurl_nocache);
                            },1)
                            """,
            fileurl="=#FORM.record.fileurl",
            _saved="^#FORM.controller.saved",
            _loaded="^#FORM.controller.loaded",
            _virtual_column="fileurl",
        )
        pane.iframe(
            height="100%",
            width="100%",
            border=0,
            src="^#FORM.record.$fileurl_nocache",
            documentClasses=True,
        )


class FormFromVolontario(FormBase):
    py_requires = "gnrcomponents/dynamicform/dynamicform:DynamicForm"

    def th_form(self, form):
        bc = form.center.borderContainer(datapath=".record")
        fb = bc.contentPane(region="top").formbuilder(cols=3, border_spacing="4px")
        fb.field("cane_id", width="10em", unmodifiable=True)
        fb.field("tipo_attivita_id", width="15em", hasDownArrow=True)
        fb.field("area_id", width="15em", hasDownArrow=True)
        fb.field("data", width="8em", unmodifiable=True)
        fb.field("ora_inizio", lbl="Dalle", width="8em")
        fb.field("ora_fine", lbl="Alle", width="8em")
        tc = bc.tabContainer(region="center", margin="2px")
        tc.contentPane(title="Questionario").dynamicFieldsPane("campi_resoconto")
        tc.contentPane(title="Resoconto libero", overflow="hidden").ckeditor(
            value="^.resoconto", toolbar="simple"
        )
        self.stampaDiario(tc.contentPane(title="Documento", overflow="hidden"))

    def th_options(self):
        wd = self.db.workdate
        data_limite_inizio = datetime(wd.year, wd.month, wd.day, 0, 0)
        data_limite_inizio -= timedelta(days=7)

        return dict(
            dialog_parentRatio=0.9,
            defaultPrompt=dict(
                title="Nuovo diario",
                fields=[
                    dict(
                        value="^.unita_id",
                        tag="dbselect",
                        lbl=u"In programma",
                        width="15em",
                        dbtable="bau.pg_unita",
                        condition="""@staff_coinvolti.staff_id=:env_staff_id AND @tipo_attivita_id.richiede_diario AND
                                                                    @programma_id.__del_ts IS NULL AND
                                                                   (@diari_volontari.staff_id IS NULL OR @diari_volontari.staff_id!=:env_staff_id) AND
                                                                   $data BETWEEN :data_limite_inizio AND :env_workdate""",
                        auxColumns="$data,$ora_inizio,$ora_fine",
                        hasDownArrow=True,
                        condition_data_limite_inizio=data_limite_inizio.date(),
                        selected_data=".data",
                        selected_ora_inizio=".ora_inizio",
                        selected_tipo_attivita_id=".tipo_attivita_id",
                        selected_area_id=".area_id",
                        order_by="$data",
                        selected_ora_fine=".ora_fine",
                        selected_cane_id=".cane_id",
                    ),
                    dict(
                        value="^.cane_id",
                        tag="dbselect",
                        lbl="Cane",
                        width="15em",
                        validate_notnull=True,
                        dbtable="bau.cane",
                        hasDownArrow=True,
                    ),
                    dict(
                        value="^.tipo_attivita_id",
                        tag="dbselect",
                        condition="$child_count=0 AND $richiede_diario",
                        validate_notnull=True,
                        lbl=u"Attività",
                        width="15em",
                        dbtable="bau.tipo_attivita",
                        hasDownArrow=True,
                    ),
                    dict(
                        value="^.data",
                        tag="dateTextBox",
                        lbl="Data",
                        width="7em",
                        validate_notnull=True,
                    ),
                ],
                doSave=True,
            ),
            modal=True,
            autoSave=True,
        )


class Form(FormBase):

    py_requires = "gnrcomponents/dynamicform/dynamicform:DynamicForm"

    def th_form(self, form):
        bc = form.center.borderContainer(datapath=".record")
        fb = bc.contentPane(region="top").formbuilder(cols=4, border_spacing="4px")
        fb.field("staff_id", width="10em", unmodifiable=True, colspan=2)
        fb.field("tipo_attivita_id", width="10em", unmodifiable=True, colspan=2)
        fb.field("data", width="8em", unmodifiable=True)
        fb.field("ora_inizio", lbl="Dalle", width="8em")
        fb.field("ora_fine", lbl="Alle", width="8em")
        tc = bc.tabContainer(region="center", margin="2px")
        tc.contentPane(title="Questionario").dynamicFieldsPane("campi_resoconto")
        tc.contentPane(title="Resoconto libero", overflow="hidden").ckeditor(
            value="^.resoconto", toolbar="simple"
        )
        self.stampaDiario(tc.contentPane(title="Documento", overflow="hidden"))

    def th_options(self):
        return dict(
            dialog_parentRatio=0.9,
            default_staff_id=self.rootenv["staff_id"],
            autoSave=True,
        )


class FormFromCane(Form):
    py_requires = "gnrcomponents/dynamicform/dynamicform:DynamicForm"

    def th_form(self, form):
        bc = form.center.borderContainer(datapath=".record")
        fb = bc.contentPane(region="top").formbuilder(cols=4, border_spacing="4px")
        fb.field("tipo_attivita_id", width="10em", unmodifiable=True)
        fb.field("data", width="8em", unmodifiable=True)
        fb.field("ora_inizio", lbl="Dalle", width="8em")
        fb.field("ora_fine", lbl="Alle", width="8em")
        tc = bc.tabContainer(region="center", margin="2px")
        tc.contentPane(title="Questionario").dynamicFieldsPane("campi_resoconto")
        tc.contentPane(title="Resoconto libero", overflow="hidden").ckeditor(
            value="^.resoconto", toolbar="simple"
        )
        self.stampaDiario(tc.contentPane(title="Documento", overflow="hidden"))
