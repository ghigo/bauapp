#!/usr/bin/env python
# encoding: utf-8

from gnr.web.gnrbaseclasses import TableScriptToHtml

CURRENCY_FORMAT = "#,###.00"


class Main(TableScriptToHtml):
    maintable = "bau.diario"

    def main(self):
        page = self.getNewPage()

        # principale = page.layout('principale',top=1,left=1,right=1,bottom=1,border_width=0)
        self.testata(
            page.div(
                height="15mm", style="font-size:16pt; font-weight:bold;color:#444;"
            )
        )
        self.righe(page)
        self.body.style(
            """.sezione_label{
                font-size:13pt; 
                font-weight:bold;
                text-align:left;
                color:#444;
                padding-left:5mm;
                margin-top:5mm;
            }
            .sotto_sezione{
                font-size:12pt; 
                color:#444;
                margin-top:1mm;
            }

            """
        )
        if self.record["resoconto"]:
            page = self.getNewPage()
            page.div(
                "Resoconto libero",
                style="font-size:16pt; font-weight:bold;color:#444; text-align:center;",
            )
            page.div("%s::HTML" % self.record["resoconto"])
        # self.piede(principale.row(height=10))

    def testata(self, box):
        tr = (
            box.table(width="100%", border_width=0, border_collapse="collapse")
            .tbody()
            .tr()
        )
        tr.td(
            "Diario di %s: %s con %s"
            % (
                self.field("@staff_id.nome"),
                self.field("@tipo_attivita_id.descrizione"),
                self.field("@cane_id.nome"),
            )
        )
        tr.td(self.field("data"), style="text-align:right;")

    def righe(self, page):
        subfields = self.db.table("bau.tipo_attivita").df_subFieldsBag(
            self.record["tipo_attivita_id"]
        )
        data = self.record["campi_resoconto"]
        for pagename, content in subfields.items():
            codici_attivi = [r for r in content.keys() if data[r] is not None]
            if not codici_attivi:
                continue
            page.div(pagename, _class="sezione_label")
            box = page.div(
                style="border:1px solid silver; margin:1mm;border-radius:2mm;padding:2mm;"
            )

            for c in codici_attivi:
                n = data.getNode(c)
                box.div(
                    "<b>%s:</b> %s ::HTML" % (n.getAttr("_valuelabel"), n.value),
                    _class="sotto_sezione",
                )

    def outputDocName(self, ext=""):
        n = self.getData("record.id")
        return "%s.%s" % (n.replace(".", "_").replace("/", "_"), ext)

    def getPdfPath(self, *args, **kwargs):
        result = self.tblobj.getDocumentPath(self.record, True)
        return result
