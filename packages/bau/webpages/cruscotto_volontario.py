#!/usr/bin/env pythonw
# -*- coding: UTF-8 -*-
#


""" reception_page.py """

# --------------------------- GnrWebPage subclass ---------------------------
class GnrCustomWebPage(object):
    py_requires = """public:Public,th/th:TableHandler"""
    maintable = "bau.staff"
    css_requires = "bau"

    pageOptions = {"openMenu": False, "liveUpdate": True}

    def main(self, root, **kwargs):
        root = root.rootContentPane(datapath="main", title="Attività da volontario")
        root.thFormHandler(
            datapath="main.volontario",
            formId="formVol",
            formResource="PaginaVolontario",
            table="bau.staff",
            startKey=self.rootenv["staff_id"],
        )
