# -*- coding: utf-8 -*-


class GnrCustomWebPage(object):
    css_requires = "bau"
    py_requires = "plainindex,th/th:TableHandler"

    def index_volontario(self, root, **kwargs):
        bc = root.borderContainer()
        frame = bc.contentPane(region="center").thFormHandler(
            datapath="volontario",
            formId="formVol",
            formResource="PaginaVolontario",
            table="bau.staff",
            startKey=self.rootenv["staff_id"],
            margin="2px",
            border="1px solid silver",
            rounded=6,
        )
        self.prepareBottom(bc)
