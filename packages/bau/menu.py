#!/usr/bin/env python
# encoding: utf-8
def config(root, application=None):
    # root.thpage('Eventi',tags='org',table='bau.evento')
    root.thpage("Programmi volontari ", tags="org", table="bau.pg_programma")
    root.webpage(
        "Cruscotto volontario",
        filepath="bau/cruscotto_volontario",
        tags="staff",
        checkenv="staff_id",
    )
    root.thpage(u"!![it]Eventi", table="bau.evento")

    arch = root.branch("Archivi", tags="org")
    arch.thpage(u"!![it]Staff", table="bau.staff")
    arch.thpage(u"!![it]Cani", table="bau.cane")
    arch.thpage(u"!![it]Diari volontari", table="bau.diario")
    arch.thpage(u"!![it]Assemblee", table="bau.assemblea")

    analisi = root.branch("Analisi dati", tags="org")
    analisi.thpage(u"!![it]Lavoro in canile", table="bau.pg_unita_staff")

    arch.thpage(u"!![it]Allocazione cani", table="bau.struttura")

    conf = root.branch("Configurazioni", tags="org")
    conf.thpage(u"!![it]Razze", table="bau.cane_razza")
    conf.thpage(u"!![it]Tipi attività", table="bau.tipo_attivita")
    conf.thpage(u"!![it]Farmaci", table="bau.farmaco_tipo")
    conf.lookups(u"!![it]Tabelle di configurazione", lookup_manager="bau")
    conf.thpage(u"!![it]Valutazione tipo", table="bau.cane_valutazione_tipo")
    conf.thpage(u"!![it]Esami", table="bau.esame")

    root.branch("Amministrazione sito", tags="superadmin,_DEV_", pkg="adm")
    root.branch("Sistemistica", tags="_DEV_", pkg="sys")
    root.branch("Organizer", tags="_DEV_", pkg="orgn")
    # root.branch('Bot',tags='org',pkg='genrobot')

    # root.branch('Geo italia',tags='_DEV_',pkg='glbl')

    # root.thpage(u"!![it]Mansioni", table="gesser.mansione")
