#!/usr/bin/env python
# encoding: utf-8
from gnr.app.gnrdbo import GnrDboTable, GnrDboPackage


class Package(GnrDboPackage):
    def config_attributes(self):
        return dict(
            comment="rescueleg package",
            sqlschema="rescueleg",
            sqlprefix=True,
            name_short="Rescueleg",
            name_long="legacy",
            name_full="Rescueleg",
        )

    def config_db(self, pkg):
        pass


class Table(GnrDboTable):
    pass
